% Stateless first attempt

# What I've got
The current version of the system is capable of live updating code when a file
is saved; it is capable of updating arbitrary stateless code (note that due to
classes being inherently stateful this means they cannot be used), including
animations as a function of time. Unfortunately at the moment any changes simply
cause the running program to be re-executed from the start. As the current
system only supports stateless programs this is not an issue but it will need to
be rectified in future.

# Limitations
Besides the aforementioned statelessness and reset-on-update, the system
enforces a particular program structure on the programmer. This is somewhat
sensible as only certain types of program are expected to be created in this
system and this enforces those types of system on the user.

The system requires that the programmer provide a "init_once" function that will
never be updated; this is counter to a goal of this project: "provide as
seamless/pythonic an experience to the programmer as possible" (not that I've mentioned
this yet, but I've always thought it). A better solution would be to use
decorators in some way to mark things as non-updating. We could extend this
with using decorators to enforce update styles.

The system cannot currently import code from a submodule. The reason for this is
unkown but presumed to be a peculiarity of Python's import mechanism interacting
with the currently very hacky system code. This needs investigating.

# Example code
Following is the most basic ball-drawing example that can be modified to add
time-based animations and the like.

~~~~{.python}
import pygame


def init_once():
    pygame.init()
    screen = pygame.display.set_mode((640, 480))

    return (screen,)


def init():
    # Some colours
    black = (0, 0, 0)
    purple = (116, 4, 181)

    ball_pos = (400, 120)
    radius = 40

    return (black, purple, ball_pos, radius)


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)


def draw(screen, black, purple, ball_pos, radius):
    pygame.draw.circle(screen, darken(purple), ball_pos, radius)
    pygame.draw.circle(screen, purple, ball_pos, radius, 1)

def do_frame(screen, black, purple, ball_pos, radius):
    screen.fill(black)
    draw(screen, black, purple, ball_pos, radius)
    pygame.display.flip()
~~~~

The most simple modification one can make is changing the ball radius, position,
or colour. But it is also possible to introduce new functions and modify the
behaviour of the existing functions. For instance changing the amount by which
colours are darkened.

The most advanced code that I have produced (in a single
updating session) from the above code is:

~~~~{.python}
import pygame

from math import cos, sin, tan, fmod
import time


def init_once():
    pygame.init()
    screen = pygame.display.set_mode((640, 640))
    clock = pygame.time.Clock()

    return (screen, clock)

def init():
    # Some colours
    black = (0, 0, 0)
    purple = (255, 50, 80)

    yellow = (0, 0, 0)

    ball_pos = (100, 100)
    radius = 100

    return (black, purple, yellow, ball_pos, radius)


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)

def invert((R, G, B)):
    """ Invert a colour """
    return (255 - R, 255 - G, 255 - B)


def draw(screen, black, purple, ball_pos, radius):
    pygame.draw.circle(screen, purple, ball_pos, radius + 5) 
    pygame.draw.circle(screen, darken(purple), ball_pos, radius)

def rad((x, y), radius):
    return abs(int(round((y / 640.0) * radius)))

def make_pos((x, y)):
    return int(round(x)), int(round(y))

def do_frame(screen, clock, black, purple, yellow, ball_pos, radius):
    t = time.time() * 1.5
    black = ((sin(t) * 28) + 127, 255 - ((sin(t) * 28) + 127), (cos(t) * 28) + 127)
    purple = ((cos(t) * 128) + 127, (sin(t) * 128) + 127, 255 - ((sin(t) * 128) + 127))

    (x, y) = ball_pos
    x = (sin(t) * 320) + 320
    y = (cos(t) * 40) + 240

    p1 = make_pos((x, y))
    p2 = make_pos((640 - x, 480 - y))
    p3 = make_pos((640 - y, 480 - x))
    p4 = make_pos((y, x))

    screen.fill(black)
    draw(screen, black, purple, p1, rad(p1, radius))
    draw(screen, black, invert(purple), p2, rad(p2, radius))

    pygame.display.flip()
    clock.tick(60)
~~~~

This was performed incrementally and is obviously badly thought-out code. But it
demonstrates how much can be changed.
