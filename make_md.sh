#!/bin/sh
template=${1%.md}_template.tex
bibliography=${1%.md}.bib
output=${1%.md}.pdf
pandoc_binary=pandoc
echo $output
if [ -e $template ]
then
    t=--template=$template
else
    t=''
fi
if [ -e $bibliography ]
then
    b="--bibliography=$bibliography --csl=./ieee.csl"
else
    b=''
fi
cmd="$pandoc_binary --toc -V papersize:\"a4paper\" $t $b -o $output $@"
echo $cmd
$cmd
