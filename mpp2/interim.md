% MInf Phase 2 Interim Progress Report
% Kit Barnes - s0905642
% January 2014

# Project and goals[^1]
A typical programming workflow consists of the programmer:

1. Making some changes to the code
2. Compiling that code
3. Running the code to see the changes

In interpreted languages we skip step two but we must still run our changed code
to see how the output has changed. This presents three problems:

1.  This is an interruption that removes the programmer from his code, however
    briefly. It is well known that interruptions have strong negative effects on
    programmer productivity; typically causing the programmer to cease work for
    multiple minutes[@Peopleware].
2.  In the case of a long running and highly stateful process such as a server
    for a computer game it is desirable that we avoid any interruption to the
    service provided by that server. As a result the process of compilation and
    re-running the server to perform an update is something we wish to eliminate;
    instead it is desirable to apply the update to the running process with no
    discontinuity of service.
3.  In creative programming works -- such as computer games (or indeed any
    programming according to some) -- this interruption stints the exploratory,
    creative work that the programmer is doing. Compare for instance the classic
    image manipulation tools ImageMagick and Photoshop:
     - In ImageMagick you start with an image and write a command that will in
       some way manipulate it. This requires that you envisage the change
       yourself.
     - In Photoshop most of the available tools are applied onto the image in
       real-time. This allows the user to "ask the computer" what a change will
       look like rather than having to imagine it themselves.

    In the context of programming computer games we can imagine a programmer
    playing with the value and behaviour of gravity to find a value which
    "feels right" -- an important property for a game. Or even a programmer
    exploring different behaviours for player motion to discover fun mechanisms.

    All of these activities are much harder than necessary due to the compile
    and run cycle.

For each of these problems I have identified potential solutions:

1. To prevent the programmer from having to take his mind off of the code at any
   point we must automatically run the code as seamlessly as possible. An
   example of this would be a simple Eclipse plugin that compiles and runs the
   current code whenever the user saves (or possibly when they stop typing).
2. To allow a long-running and highly stateful process to be updated we can draw
   upon the work of Hicks et al in the Dynamic Software Updating (DSU) discipline
   [@DSU].
3. Combining the above two to produce a system that will automatically inject
   new behaviours and values into an already running program requiring minimal
   interaction from the programmer. An example of this would be a game engine
   that listens for changes to the source files that comprise it and updates
   using DSU techniques when it detects a change.  This would result in a
   workflow where the user simply writes code and can immediately see the
   changes.

This third task is what this project undertakes to complete.

## Goals
The major goals of this project are:

* To produce a system (hereafter instapy[^3]) that can update a running game
  in-place, maintaining the state of the game between code versions.
* That instapy will follow the principle of least surprise in the hopes that
  the programmer will not have to expend mental capacity on considering the
  behaviour of the system.
* For example in the case of updates with multiple ways of maintaining the state
  of the game throughout: instapy should perform the one expected by the
  programmer.
* instapy will provide a programming interface allowing for user interfaces
  to be produced for common editors and IDEs.

# Work done
## January - July 2013
In the first year of work on this project a system was produced that could
successfully update a game written in Python with some restrictions on how that
game was written:

#### Structure
instapy requires that the game provide a class containing:

* A `loop_body` function that will be executed repeatedly to perform the desired
  computation.
* An `init` function that will be used to set up the initial state of the game.
* An `init_once` function that will perform initialisation actions that can only
  ever occur once (e.g. the `pygame.init()` function).

#### Model of computation
For a game to be updated by this version of instapy it could not use an
object oriented model of computation. All code had to be written in a
procedural style with global state.

#### Packages
instapy could not update modules imported from packages so it was necessary
for all game code to reside in the same directory.

#### Interface
instapy would update the running game whenever the source file was changed; as a
result the programmer could initiate an update by saving the file in their
editor or IDE of choice. Other than this no interaction with the system was
possible.

## September 2013 - January 2014
In the first semester of the second year of the project instapy was improved
in a number of ways:

#### Model of computation
The programmer can now make use of an object oriented style of programming,
though not without limitation. The system can update:

* The fields of objects that are present immediately after the object is
  initialised.
* The methods of an object
* Within complex object structures

It cannot perform updates that change the structure of objects (e.g.
converting between complex data types).

#### Packages
The programmer can now import arbitrarily using Python's default import
mechanism. Replacement (user defined) import mechanisms cannot be handled,
however.

#### Interface
instapy now presents a rudimentary graphical user interface that allows the
programmer to toggle the automatic update functionality and initiate updates
manually.

# System description
## Example transformations
### Basic value changes
instapy can perform updates that change the initial values of fields within
objects. It will not modify values that are not changed but if a value has
changed since the program was started then it will be set to the new value.
For example[^2]:

> ~~~~{.python}
>  # ...
>  class Game(object):
>      # ...
>      def init(self):
>          # ...
> -        self.ball_x = 320
> +        self.ball_x = 100
>          # ...
>  # ...
> ~~~~

### Function changes
Arbitrary changes to the behaviour of methods and functions are possible and the
running game will reflect the new behaviour:

> ~~~~{.python}
>  # ...
>  class Game(object):
>      # ...
>      def update(self):
>          # ...
> -        self.ball_y += gravity * dt_ratio
> +        self.ball_y -= gravity * dt_ratio
>          # ...
>  # ...
> ~~~~

### Structural changes
instapy allows the programmer to move functions and values arbitrarily between
modules and even introduce new modules:

Starting in one file:

#### `game.py`
> ~~~~{.python}
>  # ...
>  def darken((r, g, b)):
>      return (r / 4, g / 4, b / 4)
>  # ...
>  darken(red)
>  # ...
> ~~~~

Then turning into two files:

#### `game.py`
> ~~~~{.python}
>  import colour
>  # ...
>  colour.darken(red)
>  # ...
> ~~~~

#### `colour.py`
> ~~~~{.python}
>  def darken((r, g, b)):
>      return (r / 4, g / 4, b / 4)
> ~~~~

### Using objects as record types
If objects are used merely as a way of storing named values then arbitrary
transformations are possible.

For example changing the way we use an object:

> ~~~~{.python}
>  # ...
>  class Point(object):
>      def __init__(self, x, y):
>          self.x = x
>          self.y = y
>  
>  class Game(object):
>      # ...
>      def init(self):
>          # ...
> -        self.ball_position = Point(120, 120)
> +        self.ball_position = Point(64, 64)
>  # ...
> ~~~~

Or changing the way an object works

> ~~~~{.python}
>  # ...
>  class Point(object):
>      def __init__(self, x, y):
> -          self.x = x
> -          self.y = y
> +          self.coordinates = (x, y)
>  
>  class Game(object):
>      # ...
>      def init(self):
>          # ...
>          self.ball_position = Point(120, 120)
>  # ...
> ~~~~

### More complex objects
If, on the other hand, objects are given methods then updates are limited in the
same ways as updates for the main game object are.

### Objects within objects
instapy can perform updates on programs with arbitrarily nested structures of
objects:

We can change the values and behaviour of the objects at any level:

> ~~~~{.python}
>  # ...
>  class Point(object):
>      def __init__(self, x, y):
> -        self.coordinates = [x, y]
> +        self.x = x
> +        self.y = y
>  # ...
>  class Ball(object):
>      def __init__(self, x, y):
>          self.position = Point(x, y)
>  
>      def update(self, dt_ratio):
>          # ...
> -        self.position.coordinates[1] += gravity
> +        self.position.y -= gravity
>          # Note in this change we are changing both the behaviour
>          # of this method and the way of accessing the 'y' property
>          # ...
>  # ...
>  class Game(object):
>      def init(self):
> -        self.ball = Ball(120, 120)
> +        self.ball = Ball(20, 20)
>  # ...
> ~~~~

## Limitations
The following limitations are far from an exhaustive list of transformations
that instapy cannot apply. They are however representative of common scenarios.

### Object structure limitations
A key limitation of instapy is that it cannot maintain state when the structure
of objects itself is changed. For example if the programmer were to attempt the
following update then instapy would completely reset the state of the
`ball_position` variable:

> ~~~~{.python}
> +class Point(object):
> +    # ...
>  # ...
>  class Game(object):
>      # ...
>      def init(self):
>          # ...
> -        self.ball_position = (120, 120)
> +        self.ball_position = Point(120, 120)
>          # ...
> ~~~~

This is as instapy cannot recognise that the `Point` class is analogous to the
original tuple.

### Example: Bitmap caching
A common pattern in vector-drawn games is to have the object compute its
bitmap only if the underlying vector has changed (as the drawing computation is
costly). This can be achieved by caching the bitmap like so:

> ~~~~{.python}
>  class Ball(object):
>      # ...
>      def _make_drawable(self):
>          # ...
>          # Create and return the bitmap
>  
>      @property
>      def drawable(self):
>          try:
>              return self._drawable
>          except AttributeError:
>              self._drawable = self._make_drawable()
>              return self._drawable
>  # ...
>  class Game(object):
>      # ...
>      def draw(self):
>          # ...
>          self.screen.blit(self.ball.drawable, ...)
>  # ...
> ~~~~

In this example Python's `@property` decorator was used to produce a method that
is accessed as though it were a field. The `ball` object will only have a
`_drawable` field after the `drawable` property has been accessed at least once.
As a result instapy will not "notice" that a change to the `_make_drawable`
method should result in the drawable being recreated and the appearance in the
game will not change. A simple workaround in the game is to have the `__init__`
method draw the bitmap.

### Lists and references not stored as fields
instapy will fail to maintain state between modifications of lists:

> ~~~~{.python}
>  #...
>  class Game(object):
>      def init(self):
>          # ...
> -        self.weapons = [Pistol(), MachineGun(), Laser()]
> +        self.weapons = [Pistol(), MachineGun(), Laser(), Bomb()]
>          # ...
> ~~~~

In the above transformation all weapon objects will be reset to their initial
states. The reason for this is that instapy will only traverse links between
objects that are accessed as fields, not elements (in Python syntax: instapy can
only find objects accessed via a `.`, not via `[]`).

### Renaming fields
If the programmer should rename a field then instapy will be unable to maintain
state between updates. This occurs because instapy uses the name of a field as
the identity between versions of the code. The following update would cause a
reset in the state of the  variable, for example:

> ~~~~{.python}
>  # ...
>  class Game(object):
>      # ...
>      def init(self):
>          # ...
> -        self.ship = Player()
> +        self.player = Player()
>  # ...
> ~~~~

## Mechanism
instapy makes use of Python's dynamic nature to allow it to:

* Dynamically recompile modules
* Update the behaviour of an object at run time
* Access the internals of the object creation mechanism

instapy's `play` function works as such:

~~~~{.numberLines}
Initialize the game object
Loop until done:
    if we have been notified of a changed file:
        run the update process
    run the game object's loop function
~~~~

Where the user writes "the game object's loop function". This requirement --
that the looping itself be performed by the system, rather than the game -- is a
crucial property of this system that allows the system to perform updates
between frames, avoiding race conditions and giving the system execution time. A
considered alternative mechanism was to augment all user functions to perform
the check themselves.

The game update mechanism works as follows:

~~~~{.numberLines}
Acquire a new version of the module that holds the main game class
Add the main game object to the frontier
Traverse the frontier, using the object update mechanism to update each node
Replace all loaded modules with updated versions
~~~~

And the object update mechanism works as follows:

~~~~{.numberLines}
Initialize a new object using the new code
Initialize a new object using the old code
Update any properties that differ between these two objects
Update the methods of the object from the new object
Add all sub-objects to the frontier
~~~~

The frontier is a set that uses object identity (i.e. memory location) as the
equivalence mechanism. This ensures that every object is updated once and only
once.

Besides these functions the system also runs an XMLRPC interface that is used to
initiate updates from an external process. The currently implemented GUI is one
such external process that will watch the filesystem for changes and send an
update request to the main process when a file is saved. The GUI will also allow
the user to toggle the automatic update behaviour in favour of a manual update
button.

If at any point the running game throws an uncaught exception it will be caught
by instapy which will then open a debugger at the location in the stack that the
exception was originally thrown from.

# Remaining work
In its current state instapy is a useful system for developing games in Python.
However using it effectively requires that the programmer have some knowledge of
the mechanism by which it works to allow them to reason about its behaviour. A
goal of the project is to allow the programmer to use the system without such
cognitive load.

To this end there are two important pieces of future work:

1. Add a mechanism for specifying specialised update procedures. This would take
   the form of a method on an object that could be overridden.
2. Add an inference mechanism that would automatically create such methods.

The inference mechanism had been planned as a part of this project but other
aspects of the project required longer than expected so it is now relegated to
further work.

For the user interface of the system plugins will be produced for both a text
editor and an IDE. The text editor plugin will support initiating updates at a
more fine grain than the current "on file saved" mechanism. For example it may
initiate an update when the programmer has not typed for a small period of time
and the code is syntactically correct.

The IDE plugin will hook into the IDE's "run" mechanism and provide a similar
interface to the text editor plugin.

## Timeline
The following timeline lists work that will be completed by the *end* of each
month (unless otherwise noted).

---------------------------------------------------------------------
Month       System improvements    Interface          Writeup
---------  ---------------------  --------------     ----------------
January                                              This report

February    User-provided          Text editor       Continual work
            transformations        plugin            on final report

March                              IDE Plugin        Draft of final
                                                     report mid-March

April                                                Hand in final
                                                     report

---------------------------------------------------------------------

\newpage

# Proposed contents for final report
* Abstract
* Introduction (project and goals)
* Related Literature
* System description (as it is now)
* Work done (including how the project has changed)
* Evaluation
* Conclusion
* Bibliography

\newpage

# References

[^1]: Until "Goals" this section is copied from last year's final report. The
      goals section makes explicit the goals I have been assuming for the
      duration of the project.
[^2]: Code examples will use a diff-like syntax with a `-` at the front of the
      line to represent a line removed and a `+` at the front to represent a
      line added. `# ...` means irrelevant lines have been omitted.
[^3]: This is a non-capitalised proper noun, not a spelling error.
