In this chapter we give evidence for and discuss whether this project has been
successfully carried out.

# Test cases
The primary evaluation will consist of a series of tests that have been run on
*instapy* as it was before this project and on the most recent version. Each test
consists of a single source file with a change written in in a diff-like format
(the line to be removed begins with `-` while the line to replace it with begins
with `+`). Additionally, for each test, the unchanged version of the source was
tested with a blank update (using the 'Update Now' button in the *instapy* GUI) as
a sanity check.

Most of the tests demonstrate new functionality that has been added during this
project, while others demonstrate that old functionality has not been damaged
during the project. One test revealed a regression in a previously fixed bug.

Each test consists of four or five sections to aid clarity:

1. A description of what the test is for, how it works
2. The code for the test (in the previously described diff-like format)
3. How *instapy* would behave on this test prior to this project
4. How *instapy* now behaves on this test
5. Optionally extra notes

\newpage

## Values
#### Examines the behaviour when:
A value stored within an object is changed.

#### Code:
This program prints that values of a list and the first value is incremented
each time.

~~~~{.python}
 import time

 from instapy import reloader


 class SomeClass(object):
     def __init__(self):
         self.a = 1
-        self.b = 2
+        self.b = 4

     def print_it(self):
         self.a += 1
         print "a: %d\nb: %d" % (self.a, self.b)


 class Test(reloader.Looper):
     def __init__(self):
         self.instance = SomeClass()

     def loop_body(self):
         self.instance.print_it()
         time.sleep(0.5)
~~~~

#### Old result:
The `b` value will be changed but so will the `a` value as the entire object has
been replaced.

#### New result:
The `b` value alone will be replaced.

#### Notes:
In *all* following tests with a counter that is reset by the old version of
*instapy* this counter will be reset *even if the update changes nothing*.

## Functions
#### Examines the behaviour when:
The programmer changes the behaviour of a function.

#### Code:
This program prints the value of a function applied to the number 20.

~~~~{.python}
 import time

 from instapy import reloader


 def some_function(argument):
-    return argument * 12
+    return argument * 120


 class Test(reloader.Looper):
     def __init_once__(self):
         pass

     def __init__(self):
         pass

     def loop_body(self):
         print some_function(20)
         time.sleep(0.5)

     def __instapy_update__(self, old, new):
         pass
~~~~

#### Old result:
The function will begin multiplying by 120 instead of 12

#### New result:
The function will begin multiplying by 120 instead of 12

#### Notes:
This shows that function updates continue to work and should act as contrast to
the next test.

## Object Methods
#### Examines the behaviour when:
The programmer changes the behaviour of a *method*.

#### Code:
This program prints "A Thing" repeatedly.

~~~~{.python}
 import time

 from instapy import reloader


 class SomeClass(object):
     def print_a_thing(self):
-        print "A Thing"
+        print "Not A Thing"


 class Test(reloader.Looper):
     def __init_once__(self):
         pass

     def __init__(self):
         self.some_instance = SomeClass()

     def loop_body(self):
         self.some_instance.print_a_thing()
         time.sleep(0.5)

     def __instapy_update__(self, old, new):
         pass
~~~~

#### Old result:
The program will begin printing "Not A Thing" rather than "A Thing" but the
entire object will also be replaced.

#### New result:
The program will begin printing "Not A Thing" rather than "A Thing"

#### Notes:
Further tests follow to differentiate between replacing the entire object and
updating its methods.

## Functions with state maintenance
#### Examines the behaviour when:
The programmer changes the behaviour of a function while maintaining the state
of the root object.

#### Code:
This program prints the value of a function applied to a 20. Additionally it
prints a counter incrementing from 1000.

~~~~{.python}
 import time

 from instapy import reloader


 def some_function(argument):
-    return argument * 12
+    return argument * 120


 class Test(reloader.Looper):
     def __init_once__(self):
         pass

     def __init__(self):
         self.mangle = 20
         self.leave = 1000

     def loop_body(self):
         self.leave += 1
         print some_function(self.mangle)
         print self.leave
         time.sleep(0.5)

     def __instapy_update__(self, old, new):
         pass
~~~~

#### Old result:
The function will update without touching the `mangle` field.

#### New result:
The function will update without touching the `mangle` field.

#### Notes:
This is a typical example of the limits of what *instapy* could update before this
project.

## Object Methods with state maintenance
#### Examines the behaviour when:
The programmer changes the behaviour of a method in an object that has state.

#### Code:
This program prints "This bit changes" and a counter incrementing from 1.

~~~~{.python}
 import time

 from instapy import reloader


 class SomeClass(object):
     def __init__(self):
         self.value = 1

     def print_value(self):
-        print "This bit changes"
+        print "This bit has changed"

         print "The value is currently at %d" % self.value
         self.value += 1


 class Test(reloader.Looper):
     def __init_once__(self):
         pass

     def __init__(self):
         self.some_instance = SomeClass()

     def loop_body(self):
         self.some_instance.print_value()
         time.sleep(0.5)

     def __instapy_update__(self, old, new):
         pass
~~~~

#### Old result:
The `value` field of `some_instance` would be reset to $1$, though the behaviour
would update. This would happen as, again, the entire object would be replaced.

#### New result:
The `value` field will continue its count without resetting whilst the behaviour
of the method will be updated.

## Object Initialisation Arguments
#### Examines the behaviour when:
The programmer changes the arguments passed into an object when it is created.

#### Code:
This program prints "A thing" repeatedly along with a counter incrementing from
1.

~~~~{.python}
 import time

 from instapy import reloader


 class SomeClass(object):
     def __init__(self, thing):
         self.thing = thing
         self.value = 1

     def print_a_thing(self):
         print self.thing


 class Test(reloader.Looper):
     def __init_once__(self):
         pass

     def __init__(self):
-        self.some_instance = SomeClass("A thing")
+        self.some_instance = SomeClass("Another thing")

     def loop_body(self):
         self.some_instance.value += 1
         print self.some_instance.value
         self.some_instance.print_a_thing()
         time.sleep(0.5)

     def __instapy_update__(self, old, new):
         pass
~~~~

#### Old result:
`some_instance` would begin printing "Another thing" but the `value` counter
would be reset.

#### New result:
`some_instance` would begin printing "Another thing".

## Maintaining state on objects in deeper levels
#### Examines the behaviour when:
An object nested within another object is updated.

#### Code:
This program prints "This bit changes" along with a counter incrementing from 1.

~~~~{.python}
 import time

 from instapy import reloader


 class SomeClass(object):
     def __init__(self):
         self.value = 1

     def print_value(self):
-        print "This bit changes"
+        print "This bit has changed"

         print "The value is currently at %d" % self.value
         self.value += 1


 class OuterClass(object):
     def __init__(self):
         self.inner = SomeClass()


 class Test(reloader.Looper):
     def __init_once__(self):
         pass

     def __init__(self):
         self.outer = OuterClass()

     def loop_body(self):
         self.outer.inner.print_value()
         time.sleep(0.5)

     def __instapy_update__(self, old, new):
         pass
~~~~

#### Old result:
The behaviour will update but the counter will be reset as the entire object is
replaced.

#### New result:
The behavior will update without affecting the counter.

#### Notes:
Any level of nesting will work, as will changes to values.

## Maintaining state on an object graph with loops in it
#### Examines the behaviour when:
A change is made to an object containing a reference to an object that refers to
it.

#### Code:
This program prints "It's a thing" as well as a counter incrementing from 1.
Additionally it prints a boolean value representing whether the parent of an
instance is the same object as the object that instantiated it.

~~~~{.python}
 import time

 from instapy import reloader


 class HasLoop(object):
     def __init__(self, parent):
         self._parent = parent
         self.value = 1

     def print_a_thing(self):
         self.value += 1
         print self._parent.thing
         print self.value


 class Test(reloader.Looper):
     def __init_once__(self):
         pass

     def __init__(self):
-        self.thing = "It's a thing"
+        self.thing = "It's another thing"
         self.hasloop = HasLoop(self)

     def loop_body(self):
         self.hasloop.print_a_thing()
         print self.hasloop._parent is self
         time.sleep(0.5)

     def __instapy_update__(self, old, new):
         pass
~~~~

#### Old result:
The text will change but the counter will be reset *and* the second printed line
will change from "True" to "False" as the parent object referred to by the
instance is no longer the root game object.

#### New result:
The text will change without resetting the counter or disconnecting the instance
from its parent object.

#### Notes:
More importantly this demonstrates that *instapy*'s graph traversal mechanism does
not get caught in loops. Running this on a particular commit of *instapy* that
does *not* detect loops will result in an infinite loop.

## Using the `__instapy_update__` method
#### Examines the behaviour when:
The programmer uses the `__instapy_update__` method to ensure that a cached
value is correctly recalculated after an update.

Notably this uses Python's `@property` decorator to produce a function that can
be accessed as though it were a field (this function implementing the caching
behaviour).

#### Code:
This program prints "A thing" repeatedly along with a counter incrementing from
1.

~~~~{.python}
 import time

 from instapy import reloader


 class SomeClass(object):
     def __init__(self):
         self.value = 1

     def print_a_thing(self):
         self.value += 1
         print self.thing
         print self.value

     @property
     def thing(self):
         try:
             return self._thing
         except AttributeError:
-            s = "A thing"
+            s = "Another thing"
             self._thing = "%s %s" % (s, time.time())
             return self._thing

     def __instapy_update__(self, old, new):
         if old.thing != new.thing:
             del self._thing


 class Test(reloader.Looper):
     def __init_once__(self):
         pass

     def __init__(self):
         self.some_instance = SomeClass()

     def loop_body(self):
         self.some_instance.print_a_thing()
         time.sleep(0.5)

     def __instapy_update__(self, old, new):
         pass
~~~~

#### Old result:
The output will change but the counter will be reset.

#### New result:
The output will change without resetting the counter.

#### Notes:
This test was distilled from the asteroids game example which was not written
specifically as an example for *instapy* but as an independent game *using*
*instapy*.

## `__init_once__`
#### Examines the behaviour when:
The game object defines an `__init_once__` method.

#### Code:
In normal operation this program prints nothing. If the `__init_once__` function
is not called or is called more than once then the program will raise an
exception.

~~~~{.python}
from instapy import reloader
import singleton


class Test(reloader.Looper):
    def __init_once__(self):
        assert not singleton.init_onced
        singleton.init_onced = True

    def __init__(self):
        pass

    def loop_body(self):
        assert singleton.init_onced

    def __instapy_update__(self, old, new):
        pass
~~~~

With an additional file `singleton.py`:

~~~~{.python}
init_onced = False
~~~~

#### Old result:
The assertion in `loop_body` will fail before an update is even run as the
`__init_once__` method has never been run.

#### New result:
The assertion in `loop_body` will succeed and updates will not re-call the
`__init_once__` method so the assertion in that method will never raise an
exception.

#### Notes:
We use a separate module as otherwise the assertion in `__init_once__` would
always succeed (because each reloading `Test` object would have access only to
the version of `init_onced` in its own module). Putting the variable in a
different module causes it to be updated independently.

This test has no changes as it tests simply that the update mechanism
successfully avoids calling `__init_once__` multiple times, even on different
objects of the same class.

## Lists containing objects
#### Examines the behaviour when:
The programmer makes changes to an object stored within a list.

#### Code:
This program prints "An element called {name} seen {number} times" for each of
two objects. The "seen" number increments.

~~~~{.python}
 from instapy import reloader
 import time


 class Element(object):
     def __init__(self, name):
         self.seen = 0
         self.name = name

     def print_it(self):
         self.seen += 1

-        s = "An element"
+        s = "Another element"
         print "%s called %s seen %d times" % (s, self.name, self.seen)


 class Test(reloader.Looper):
     def __init_once__(self):
         pass

     def __init__(self):
         self.pair = [Element("Alice"), Element("Bob")]

     def loop_body(self):
         for e in self.pair:
             e.print_it()
         time.sleep(1)
~~~~

#### Old result:
The program will begin printing "Another element" rather than "An element" but
both counters will be reset.

#### New result:
The program will begin printing "Another element" rather than "An element".

## Lists containing values
#### Examines the behaviour when:
The programmer makes changes to a list that contains values.

#### Code:
This program prints the value of a list as the first element of this list is
incremented from 1.

~~~~{.python}
 from instapy import reloader
 import time


 class Game(reloader.Looper):
     def __init_once__(self):
         pass

     def __init__(self):
-        self.pair = [1, 24]
+        self.pair = [1, 23]

     def loop_body(self):
         self.pair[0] += 1
         print self.pair
         time.sleep(1)
~~~~

#### Old result:
The entire list is replaced, resetting the counter.

#### New result:
The entire list is replaced, resetting the counter.

#### Notes:
This is intentional behaviour.

## Modules in game object initialisation
#### Examines the behaviour when:
The programmer uses imported modules during the initialisation methods of their
game object.

#### Code:
This program repeatedly prints pi.

~~~~{.python}
 import math
 import time

 from instapy import reloader


 class Printer(reloader.Looper):
     def __init__(self):
         self.output = math.pi

     def loop_body(self):
         print self.output
         time.sleep(0.5)
~~~~

#### Old result:
During the update the *old* instance would raise exceptions as the modules it
attempted to use had been deleted.

#### New result:
The output will remain the same (this update includes no change to the code) but
no exceptions will be raised.

#### Notes:
This test was produced explicitly to find the simplest example that would cause
a bug. The bug was solved by keeping a history of generations of modules.

## Importing packages
#### Examines the behaviour when:
The programmer imports code from a package.

#### Code:
This program repeatedly prints 1.

~~~~{.python}
 import time

 from instapy import reloader

 import test_package


 class Test(reloader.Looper):
     def __init__(self):
         pass

     def loop_body(self):
         print test_package.some_value
         time.sleep(0.5)
~~~~

Additionally the directory `test_package` contained a file name `__init__.py`
with the following code in it (again in diff-like format):

~~~~{.python}
-some_value = 1
+some_value = 2
~~~~

#### Old result:
The imported module would not be found, raising an import error.

#### New result:
The printed output will change from $1$ to $2$.

## Importing from packages
#### Examines the behaviour when:

#### Code:
This program repeatedly prints 1.

~~~~{.python}
 import time

 from instapy import reloader

 from test_package import blah


 class Test(reloader.Looper):
     def __init__(self):
         pass

     def loop_body(self):
         print blah.a_value
         time.sleep(0.5)
~~~~

The aforementioned `test_package` directory also contained a file named
`blah.py` with the following code:

~~~~{.python}
- a_value = 1
+ a_value = 2
~~~~

#### Old result:
The imported module would not be found, raising an import error.

#### New result:
The imported module would not be found, raising an import error.

#### Notes:
This functionality was working in an earlier version of *instapy*, unfortunately
there has not been time to fix this bug. Interestingly it was not noticed until
explicitly tested for as none of the tested code made use of deep imports.

## Result
Having transformed from not passing most of these tests to passing most of these
tests, *instapy* has been greatly improved over the course of this project. The
set of tests passed demonstrates the most basic and general abilities necessary
for maintaining state over a wide variety of updates.

The single test that currently fails is a regression in previously fixed
behaviour that was likely introduced by recent changes to the order in which
module updates are performed. It is unfortunate that there was not enough time
to deal with this bug as it was found only in the late stages of writing this
document.

# Gupta's framework
*instapy* can be viewed as a standard DSU system with an additional attempt at a
generalised state transformation function. In particular it avoids having to
perform the data flow techniques described in \cite{Gupta96} by adding the
constraint that the programmer must provide the `main_loop` method. While the
process is not executing the `main_loop` method it is guaranteed to be safe for
an update to occur. This could be considered somewhat similar to the simpler
system described in \cite{Gupta96} that would "[cause] the running program to
generate a signal when it reaches a satisfactory control point". In *instapy* a
programmer can "signal" a satisfactory control point by relinquishing execution.

The other major issue (besides deciding when to perform an update) described in
\cite{Gupta96} is that of how to actually perform an update, given a new program
and state transformation function. This is made moot by Python's powerful
reflection capabilities.

It follows[^1] from this that, given programmer-defined update functions at the
*root* level (overriding the `_do_update` method of `reloader.Reloader` class,
for example), *instapy* can be considered to produce valid behaviour, under the
strict definition given in \cite{Gupta96}.

## As a foundation
*instapy* provides a solid foundation for future work on immediate feedback
systems, whether their goal be to improve the provided state transformation
function (or even add a mechanism that will infer acceptable functions) or to
provide the other impressive capabilities demonstrated in the original
presentation \cite{Inventing}. It is a "battle hardened" system that has been
through many iterations to find the current mechanisms that work well. Even if
large parts of the system's logic are stripped out, it provides a good entry
point for performing an update function and can perform a zip-like operation
between the object graphs for different versions that is likely to be useful for
many types of automatic DSU[^2] (ADSU) implementation.

The particular update mechanisms selected make a good foundation for future work
themselves, where mechanisms that infer more advanced updates will not replace
these mechanisms, but will build on them and infer more complex transformations.
That is to say that the transformations performed by *instapy* will not cause
incorrect behaviour within the (appreciably large) subset of program updates
that they support but this set could be increased by the addition of further
mechanisms.

[^1]: This is not a proof nor an attempt at one.
[^2]: A term I am coining to mean an implementation that provides or infers the
      state transformation function without programmer intervention.
