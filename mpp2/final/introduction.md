This project extends a system that allows a programmer using the Python
programming language to make changes to the code of their program whilst the
program is running and have those changes immediately reflected in the running
program. This involves not only updating the future behaviour of the program but
also transforming a valid state of the program before change into a valid state
of the program after change.

In the process of developing or maintaining a piece of software a programmer
will typically be forced to restart the running program whenever they make a
change to the program's source code. Not only does this force the programmer to
perform an action (or even a sequence of actions) before they can see their
changes but unless the program they are working on is built with persistence in
mind then the program will be reset to its initial state and the programmer may
have to interact with it or wait a long time before it exhibits the behaviour
they are working on.

When working on the fluid aspects of a program such as the user interface or the
general behaviour of a video game it will often be the case that the programmer
needs to explore possible behaviours and parameters to find versions that "feel
right". Usually this will mean the programmer making small changes to the code
then re-running that code to see the changes they have made. The pause caused by
re-running the code will slow the programmer down.

If the delay is greater than a minute or so then the programmer may take around
fifteen minutes to properly resume work, whether they're exploring unspecified
behaviours or carefully implementing an exact algorithm
\cite{Peopleware}\cite{Interrupted}.

In the video that inspired this project[reference], Bret Victor asserts that the
disconnection between causes and effects is stifling to creators of all
disciplines and advocates systems of immediate feedback to bring creators closer
to directly manipulating their creation (as a sculptor does) rather than
manipulating a proxy for their creation (as a programmer does).

In aid of this this project undertook to produce one such system (the system
produced will hereafter be referred to as *instapy*).

To accomplish this goal of instant feedback *instapy* must perform three tasks:

1. Detect that the programmer has changed the source code and initiate an update
2. Update the future behaviour of the running system
3. Transform the state of the running system such that the system (now running
   new code) will continue to run sensibly.

In order to focus efforts and produce a useful system within the time available
*instapy* was limited to only working on computer games written using a specific
library.

# Previous work carried out
This project expanded upon previous work on *instapy* performed last year.

Last year a system was produced that was limited to providing immediate
feedback on systems written in a procedural style; the programmer could only
write programs using functions and global state. Though these functions and the
state were encapsulated in a god object as methods and fields, the style was
isomorphic to the procedural style. If the programmer did use objects then the
system would simply treat them as atomic values. In most cases this would result
in the objects being reset to their initial state on each update.

Due to limitations in the customised import mechanism the programmer could not
use packages or submodules within packages. The layout of the modules was
required to be flat rather than hierarchical.

# Contributions
*instapy* was extended to allow it to update programs written in an object oriented
style, rather than just a procedural style. An RPC interface was added that was
used to produce a GUI and could easily be used and extended in future to allow
for IDE and editor plugins to control *instapy*.

Following is an explicit list of contributions:

* Designed and implemented an algorithm for maintaining the state of a program
  between different versions when that program uses an object oriented style.
* Evaluated mechanisms for detecting how an object has changed between versions
  and selected appropriate one.
* Devised a simple, workable, solution for maintaining the state of lists during
  an update, though compromises had to be made here.
* Produced an API and GUI using this API for *instapy*
