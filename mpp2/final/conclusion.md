# Outcome
*instapy* has been extended to maintain the state of a program whilst updating
that program for programs that use objects. Additionally a GUI has been added
that allows the programmer more control over *instapy* and, in the process, the
interface code for the system has been extracted from the core logic, resulting
in a system for which it is easier to extend the existing interface or create
alternative interfaces.

Here is a quick list of the important achievements made during this project:

* Traversing the object graph
    - Selected this after trying other mechanisms
    - Implemented this incrementally
* Maintaining state in objects
    - Settled on using the old and new instances for comparison
    - Implemented "zipping" object graphs in order to acquire these
* Maintaining state in lists
    - Recognised the issues involved in state maintenance on lists
    - Devised an effective tradeoff solution that could be implemented rapidly
* GUI
    - Built a GUI for the system, including pause and restart functionality
    - Reduced the coupling between interface/control code and the core logic of
      the system.

Following is more detail on each important acheivement made during this project:

## Graph traversal
*instapy* needed a mechanism through which it could access all objects and
variables in the program it was updating. Accessing all variables is solved by
gaining access to all objects because variables will either be global variables
or fields of objects. Local variables within functions are a non-issue as they
are guaranteed not to be in scope when the update is performed.

Initial attempts at producing this system relied on Python's metaclasses and
mocking mechanisms (for unit testing) in order to produce a list of all objects.
After building this solution it became apparent that it could not provide a
particular behaviour required by the update function (see below) and the
solution required was a graph traversal mechanism.

A graph traversal mechanism was implemented incrementally, with each version
increasing the set of graphs that could successfully be traversed until a
generic graph traversal mechanism was produced. In hindsight a standard graph
traversal algorithm should have been implemented.

## Object update
An initial attempt at an object update was made with the metaclass mechanism for
acquiring objects. It relied on all objects having no initialisation parameters
(no arguments to `__init__` as it would produce the old and new instances for an
object by simply calling the appropriate classes with no arguments. Though work
began on keeping track of the arguments an object was instantiated with using
further metaclasses, it was quickly realised that this would not be adequate as
the arguments may change.

In response graph traversal was implemented in such a way that it kept
corresponding instance objects together so that an update could be performed.

## List update
It was then noticed that objects in lists were becoming disconnected from the
rest of the graph as the list was being replaced entirely, meaning the objects
within it were replaced with objects from the new program.

Before an attempt at fixing this was made various issues with list update were
identified, primarily that it is particularly difficult to ascertain
correspondences between instances within lists as they are not named and may be
moved or replaced entirely without it being detected by the update mechanism.

As a simple solution to this various constraints were decided for lists that
allowed an implementation to be produced in minimal time. Given more time this
implementation would be replaced with a more useful, if harder to implement,
solution.

## GUI
A GUI was produced and kept separate from the logic of the update mechanism.
This GUI is easily extendable and replaceable as it uses a simple RPC interface.


# Future Work
## Better correspondence detection
*instapy* in its current form can only detect changes if the name of an object
or variable is not changed. A more intelligent mechanism may use structural
equivalence within the object graphs and the AST for the code. An improved
solution to this problem might also improve the situation with lists as their
issue is that elements within them are anonymous.

## Graph structure changes
As *instapy* currently attempts to zip the object graphs together it assumes
that the graphs will remain essentially equivalent (though some nodes may be
removed or added; again it will not detect a renamed node or added layer of
abstraction). Again this would be solved with a more advanced equivalence
detection mechanism.

## More immediate feedback with pygame integration
An automatic DSU system was only the *basis* for an immediate feedback system as
described by Bret Victor. Since the presentation was originally given Victor has
been vocal with criticisms of immediate feedback systems that consist solely of
automatic DSU as having missed the point. ADSU is *necessary* for immediate
feedback but not *sufficient*.

Improvements in this regard would include implementing the time scrubbing and
onion skin examples demonstrated but would also require less general version of
*instapy* as the functions would be specific to pygame.

# Reflections
If we were to write *instapy* again, from scratch, we would take the following
suggestions into account:

1. *instapy* was developed organically, resulting in deeply integrated portions
   of logic that could well have been kept separate. We would start afresh by
   implementing a generic DSU system with the only specialisation involved being
   the limitation to a main loop. We would then implement our current implicit
   state transformation function on top of this DSU system.

2. In instapy we selected 2d games as a focus; this focus of application was
   useful as it allowed certain tradeoffs to be made but our additional decision
   of selecting (and building for) the pygame library was not relevant for
   *instapy* as it currently stands[^1]. Writing *instapy* from scratch, we would
   produce a more generic system with respect to libraries and workflows. When
   building this we would design it with the ability to extend it in mind
   (either by inheritance or component mechanisms). With an extensible system we
   would *then* specialise for libraries when necessary.

3. When building *instapy* we did not do any kind of formal testing. All testing
   was performed simply by running example code and attempting to break it. As a
   result *instapy* suffered from many regressions such that bugs needed fixing
   more than once (and in one case a feature was accidentally broken entirely).
   If rewriting *instapy* we would use a strategy of writing tests for every bug
   and feature before either fixing the bug or implementing the feature. We
   would ensure that these tests failed prior to writing code and that they
   passed after writing code. Additionally the tests should be performable
   with a single script so that all *previous* tests may be easily run to ensure
   no regressions occur.

[^1]: Future work on including other instant feedback mechanisms such as the
      onion skin example *would* benefit from a library selection however.
