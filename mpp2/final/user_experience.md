In this chapter we describe the behaviour of *instapy* and the ways in which a
programmer can interact with it. Most of the description is given in the form of
examples of changes that the programmer can make and the behaviour that will
result from such changes.

# Description
*instapy* provides a programmer with two mechanisms for interaction: saving a
file, and a GUI. The interactions are complementary rather than being
alternatives; the file interface provides an immediate feedback mechanism,
whilst the GUI allows the programmer to have more fine-grained control over the
system. The entire system can be seen in action in figure 3.1.

## Saving a file
When the programmer saves a source file it is detected by *instapy* and an update
is triggered. This update reloads all source files and attempts to modify the
running system such that its behaviour reflects the new source files. This
requires that it update all behaviours (e.g. functions, scripts, all executable
lines of code) and map the old state of the system onto a new state that is
valid for the new behaviour.

## Errors
If at any point an error occurs, be it a syntax error or a runtime error,
*instapy* will open a debugger at the point where the error occurs. This both
pauses execution (allowing the programmer to fix the code) and allows the
programmer to inspect the environment the bug occurred in.

## Graphical User Interface
![The system in use](figures/system_full.pdf)

The GUI (seen in figure 3.1)
allows the programmer more control over the running program and the update
process.

Primarily the GUI allows the programmer to disable the automatic update
detection in favour of triggering updates manually. This is useful in cases
where the programmer wishes to save a file without triggering an update, for
example if they wish to perform an update whose changes span multiple files.

For convenience the GUI also allows the programmer to pause and restart the
program.

## Structure constraint
*instapy* requires that the programmer provide a single class that acts as the
main class for their program. This class must inherit from *instapy*'s `Looper`
class and it can provide three methods:

#### `loop_body`
is a mandatory method that acts as the point of entry for execution of the game.
The body of this function must be the *body* of what would normally be the
game's main loop.

#### `__init_once__`
is a method that will only be called a single time when the program is first
run. This is used to allow initialisation actions that must not be performed
every update (in particular, `pygame.init()` must not be called multiple times).

#### `__instapy_update__`
will be called during an update and allows the programmer to make minor changes
to the update process. Details on this are given later.

# Examples [^1]
Following are a series of examples of changes to code that *instapy* can perform
and maintain state over. The first are examples of simple value changes that can
all be performed independently of each other while later ones delve into
behaviour and more advanced changes. Finally an example is given of producing a
complete system from a minimal system (and details on what constitutes minimal
are included).

## Bouncing ball
The first examples are all changes to a system in which a ball bounces around in
a box and is pulled down by gravity. The user can cause the ball to move using
arrow keys[^2] to change its velocity.

### Size change
![The ball changing size](figures/radius.pdf)

The ball's size is determined by a radius variable, if the programmer increases
the value of this variable and changes the size then the ball will increase in
size as seen in figure 3.2. It will do this without resetting the position or
velocity properties of the ball (or indeed any other properties).

### Position change
![The ball changing position](figures/position.pdf)

The ball's position is stored as an $x, y$ tuple. If the programmer changes
either part of this tuple then the ball will move to this new position as seen
in figure 3.3. This means that the programmer cannot independently change the
$x$ or $y$ components of the position unless they store the values
independently. This is an intentional choice as it allows the programmer to
link properties as updating together with ease. A tradeoff is made here between
providing a pragmatically useful system and avoiding cases where the programmer
must have knowledge of the system in order to use it.

If the programmer were to change the representation of the position from
cartesian to, say, polar coordinates *and* they updated all methods and
functions (drawing methods and modelling methods, typically) to use the
coordinates as polar, then the system would fail to maintain state as it cannot
detect that a conversion from cartesian coordinates to polar coordinates is
necessary. Such a detection would require exceptionally effective AI or machine
learning.

### Velocity change
![The ball changing velocity](figures/velocity.pdf)

Similar to the position, the velocity is stored as a tuple of $x$ and $y$
components. If the programmer makes changes to either of these values then the
entire velocity of the ball is changed (figure 3.4). Again this happens
independently of other properties.

### Elasticity change
![The elasticity of collisions with the ball changing](figures/elasticity.pdf)

As with the other properties, the programmer can change the elasticity of the
ball to affect its collision properties (figure 3.5).

### Gravity change
![Inverting gravity](figures/gravity.pdf)

We give examples here of a number of ways in which the programmer can change the
physics of the game with regards to gravity: by value, by behaviour, and by user
input:

#### By value
Acceleration due to gravity is stored as a property in the game much like the
elasticity of the ball. This value can be changed and the system expected to
update as normal. If the programmer were to negate the value of gravity then the
ball would start falling upwards, as in figure 3.6. The following examples
replicate this behaviour in different ways.

#### By behaviour
All previous examples have been of changes to values in a game, this examples
demonstrates changes of behaviour. Say the game models gravity with a line
looking like the following:

~~~~
ball.velocity[1] -= gravity
~~~~

and the programmer changes it to:

~~~~
ball.velocity[1] += gravity
~~~~

then we would see the same behaviour as negating the value. This change would
make sense if, for example, the programmer was using a display mechanism whose
coordinate origin was at the top left as opposed to the bottom left (which the
first version assumes). An easy mistake to make and an easy mistake to rectify.

#### From user input
![The gravity inverting on user input](figures/interactive_gravity.pdf)

The programmer could also cause the ball to invert the direction in which it
falls upon user input by adding code to the event handling mechanisms. If, say,
the programmer added a block that would detect when the space bar is pressed and
invert gravity when this happens, then we would see the behaviour in figure 3.7.

The programmer can make any change they like to the behaviour of the system if
it does not rely on a change in the state of the system.

## Asteroids
The following examples focus on a more complex demo with many types of object
and loops in the graph structure. This is a much more realistic example of a
game that *instapy* might be used on. Changes similar to those described for the
ball program will all work in the asteroids program, though anything that
affects the way objects are drawn requires use of a custom update method due to
the caching behaviour in the asteroids program (detailed later).

![The asteroid game in action](figures/asteroids.pdf)

In this game the player controls a small ship flying in an asteroid field and
can destroy the asteroids using a variety of weapons as seen in figure 3.8

Of note are three important constraints a programmer must make note of:

* All fields that are to be updated must be initialised on object creation if
  *instapy* is to detect that they have changed. This means that dynamically added
  fields will need the programmer to add code in the custom update method
  mentioned later. Note that initialisation means having a meaningful value here.
  The programmer cannot simply set all fields to have null values.
* Related to this, any dynamically changing lists require that the programmer
  provide custom update code. This is as it is infeasible for *instapy* in its
  current form to detect the correspondance between elements in different
  versions of lists.
* Lists must either contain only values or only objects, they may not mix the
  two. *instapy* treats the entire list as being the same type as the first
  element (either a value or an object, no distinction is made between data
  types) the reasons for this are discussed later.

#### Custom update methods
Because it is infeasible to adequately handle every transformation it may be
expected to perform, *instapy* allows the programmer to specify a method that
will be run on an object possibly multiple times during an update. This allows
the programmer to specify changes easily. In particular it is used for the
asteroid drawing update described below.

Note that though this allows the programmer to provide more advanced state
transformations it is generally intended for much simpler code fragments, such
as deleting cached values and making minor adjustments to ensure that updated
behaviour is exhibited. As it may run multiple times it should be an idempotent
method.

#### Behaviour of objects
![Changing the way an asteroid is drawn](figures/asteroid_change.pdf)

Much like with functions, a programmer can arbitrarily change methods as long as
those changes do not rely upon changes to the state of the system that *instapy*
cannot perform.

In the asteroids game all drawn objects have a `_make_drawable` that produces a
bitmap. The public interface to this method is the `drawable` property that,
when accessed, will either return a cached copy of the bitmap or create it and
then return it. Thus in order to update this bitmap the update must somehow
invalidate the cached bitmap.

To do this, the programmer adds a custom update method that invalidates the
bitmap cache during the update process. This is a simple, one-line, method.

With this in place the programmer can then change the `_make_drawable` method
from drawing circles to randomised polygons. When the programmer then saves we
will see the behaviour in figure 3.9.

#### Values on objects
![Moving the player by changing the arguments passed to it](figures/player_move.pdf)

The programmer may change any of the initial values of objects, including those
values that are dependent upon arguments to the constructor.

In the asteroids game the player object's position is stored in a tuple named
`position` that is populated at initialisation from an argument passed in to its
constructor. If the programmer changes the starting position passed in to the
player object then the player will move to reflect this as in figure 3.10.

#### Lists
All lists must contain either only values or only objects. The programmer may
not change the size of object lists. Additionally value lists will be updated as
units (being replaced entirely or not at all). If a list of objects changes size
dynamically then changes to state will not be reflected *but* changes to
behaviour will still occur. Additionally any class-level variables will be
updated.

In the asteroids example the player's weapons are stored as a list of objects.
The programmer can change various properties of the weapon classes available.
For example they can change the fire rate of the rocket launcher and the player
in-game will start firing much faster.

## Iterative development example
In this section we detail the development of the ball program from a minimal
program up to the program running as described above. The various stages of
development of this program can be seen in figure 3.11.

![Creating a bouncing ball from the minimal program](figures/incremental.pdf)

### Minimal program
As a program can only initialise pygame and acquire a screen object to draw on
once, the programmer cannot start from an entirely blank program; they must
prepare the one-time initialisation function before running the program.
Thankfully this method will be identical for most programs.

### Drawing a circle
The next update to the program simply adds a ball object with a draw method and
a position field and begins calling that draw method. As a result a circle
appears in the centre of the display.

A clock object is added at this stage that is used to limit the rate at which
the main loop is called by delaying at the end of every frame by a variable
amount (the clock will delay for as long as it needs to produce a frame rate of
60 frames per second). When the delay method finishes it returns the amount of
time it delayed for in milliseconds, this value is stored for later use.

### Adding velocity
Next the ball object is updated with a velocity vector and an update function
which will model the motion of the ball. The circle begins to move, though only
at a constant velocity.

### Adding gravity
Next the game object is given a gravity field and is passed into the constructor
of the ball object. As a result the ball object keeps a reference back to the
game object that created it and can access the gravity variable. It uses this
during its update function to apply acceleration by decreasing the $y$ component
of its velocity by gravity at each frame[^3].

The circle starts fall and, if left to its own devices, will fall off the bottom
of the display as no collision detection has been implemented.

### Adding collisions
The update function in the ball object is then augmented to check for collisions
with the edges of the display. If a collision occurs then the corresponding
component of the balls velocity is reversed to produce a bouncing effect. This
component is then multiplied by a large fraction (in this instance $8\over10$)
to simulate elasticity. Finally if the absolute value of either component of the
ball's velocity is below a certain level then that component is reduced to zero.
This prevents the ball from "jittering" when it comes to rest on a surface.  As
a result the ball begins to bounce off of the edges of the display as though
they were surfaces, with each bounce removing a small amount of the velocity of
the ball. Left for a while the ball will eventually come to a rest.

### Adding player control
Finally a method is added to the game object that detects keyboard input from
the user and changes the velocity of the ball in response. This allows the user
to control the ball, making it jump around.

[^1]: All figures taken from \cite{MPP1}
[^2]: The system actually uses the WASD cluster of keys as stand-ins for arrow
      keys. This is the norm for video games and it would be exceedingly
      surprising to find a game that did not use these as arrow keys.
[^3]: Multiplied by the time taken per frame as determined by the clock object.
