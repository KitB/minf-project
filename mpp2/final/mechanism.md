In this chapter we detail the mechanism by which *instapy* provides the behaviour
described in the last chapter.

# Introduction to dynamic software updating
As described in the previous chapter, DSU systems allow a programmer to update a
running process to a new version of its code in-place. They do this by accepting
new program code and a state transformation function and pausing the program
whilst they perform an update[^2]. Such systems had two vital decisions to make:

## When to update
As explained in \cite{Gupta96}, it is important that the system choose a time in
which to carry out the update so as to avoid issues like race conditions.
*instapy* solves this by enforcing the `loop_body` method as the point of entry
for execution of the game. As *instapy* controls the execution of this loop
body, it can perform additional actions between iterations of the loop. Most
importantly it can perform an update between iterations. As the only parts of
the process that may change can only be run within the `loop_body` this ensures
that no race conditions (of that nature) occur.

## How to modify a running process
An obviously key capability of a DSU system is being able to modify the state
and behaviour of a running process. In compiled languages this is a significant
consideration as adding the necessary hooks to overwrite the program can
introduce significant overhead in the run time. Thankfully Python is an
interpreted language that allows us to change nearly any value available at
run-time and even has a built in mechanism for replacing the methods of an
object.

For an immediate feedback system we have an additional concern:

## How to perform a state transition
Most DSU systems assume that the programmer will be providing a transition
function [^1] as the task of inferring one is often considered to be
intractible. In the case of production systems this is almost certainly true as
a production system will need more stringent guarantees that an update will not
introduce bugs or result in unacceptable behaviours. For systems that are in
development however it may be acceptable to incur occasional errant behaviour.

# Python specifics
*instapy* is written in and for the Python programming language. Python has a
number of features that have been beneficial in producing a system for dynamic
updating:

#### Dynamic
Python is an inherently dynamic language; anything that is not a keyword can be
rebound at runtime.

#### Class rebinding
In particular the `__class__` field of a new-style object (one that inherits
from `object`) is directly used to lookup the class-level fields of an object,
including methods. This means that all methods of an object can be updated
simply by setting its `__class__` to the new version of the class.

#### Introspection
Python has thorough support for introspection as a language feature; importantly
the local and global variables of a function can be accessed from outside that
function in another thread.

#### The `inspect` module
On top of this the standard library in Python includes a module dedicated to
performing introspection. This module allows us to easily find the source code
for a given name (be it a class, method, module, etc) and differentiate between
types of names (which we use to decide how to update a name). It will also find
the name of the module in which a variable is defined, allowing us to easily
access the new versions of things.

#### Metaclasses
Python's metaclasses allow us to modify the class creation and instantiation
mechanisms. In particular we use this to implement an `__init_once__` magic
method for the main class that performs actions that must not be repeated every
update (such as initialising pygame).

#### Namespaces are dictionaries
Because everything in Python is stored in a dictionary (or equivalently
accessible via a string lookup) it is easy, given two objects, to replace the
attributes of one object with attributes from the other.

# Note on terminology
When discussing versions of a program and values and objects from those versions
we use "old" to refer to the version of the program being replaced and "new" to
refer to the version it is being replaced with. These both specifically refer to
the pristine state of the versions of the program, so "old" will *not* be the
running program that is being updated. The running program is referred to as
"current". The current program will be a *concrete* instance of the old program
until the update mechanism is run, at which point it will be transformed into a
concrete instance of the new program. At this point the new program becomes the
old program.

When referring to objects (or values), old and new refer to pristine, freshly
initialised versions of the objects defined by the old and new programs, while a
current object will be the version of the object that is in use in the system
and will encapsulate part of the state of the system.

*instapy* updates objects in-place rather than replacing them with newer versions
of themselves.

# General outline
The programmer provides an object that encapsulates their program. We call this
the game object. This object must inherit from *instapy*'s `Looper` class. This
class is required as *instapy* occasionally passes extra arguments to the
constructure of a root class to control its creation. These are used to
implement the `__init_once__` method which is an optional method that allows the
programmer to perform some actions only once per run of the program (importantly
not happening at every update). No other classes should inherit from the
`Looper` class.

The game object must also provide a `loop_body` method that would be the body
of the program's main loop. This method acts as the entry point for execution of
that program.

Updates are initiated by an XMLRPC interface exposed by *instapy*. When the
interface's `update` method is called it simply marks the game as needing an
update. Prior to the next iteration of the game object's `loop_body` function
the update will be performed.

## Acquiring fresh modules
During the update process *instapy* must acquire new versions of each of the
modules in use. This is done by first reading in the source code for the module,
then executing it with a freshly constructed blank module's dictionary as its
namespace. This mimics Python import mechanism but allows multiple generations
of module sets to be stored. We keep multiple objects in a reloader object so
that future work may extend to include stepping back and forth between versions
of modules, or perhaps step back to a previous version if an error is
encountered.


## The outer level of the update function
The outermost method of the update process simply initialises the frontier and
then loops through it, running the inner update function on everything
encountered. It also updates all variables that are in scope in the `loop_body`
method, catching any global variables that are defined in the main module.

A rough pseudocode for this method follows:

~~~~
start a new generation in the module reloader
setup a frontier for traversing the object graph
add root_object to the frontier
until frontier is empty:
    pop an object from the frontier and update it
for name in loop_body method:
    update name
~~~~

The frontier stores objects in a dictionary from the memory location of an
object to the object itself. The keys of a dictionary can be considered as a
set, so this allows us to ensure no duplicates enter the frontier. As it mimics
a set it also means that the order in which objects are removed from the
frontier is effectively random. The frontier is filled as we go by the inner
update method.

Notably when updating the globals in the `loop_body` method this includes
modules.

# Detailed updates
Once on the frontier all objects go through the same object update function;
this functions follows this general outline:

~~~~
get the latest version of the module defining the object
replace the class of the object with the new class
call object.__instapy_update__
for name in object dictionary:
    update name
~~~~

The `update` function used in the above pseudocode acts differently depending on
the type of value stored in the name:

## Values
Values are too small to perform a stateful transformation between versions, so
we must simply replace the old value with the new value. However we do not wish
to simply replace the values of all names as this would result in a complete
reset of the state stored in such names. For example if a ball object had
velocity and position values then such a scheme would reset these values when
performing *any* update. Instead we first check to see if the initial value has
changed from the old version to the new version using the initial objects. If
the new value differs from the old value then the user has explicitly changed
this value and probably wishes to see the new value in action, so we replace
this value. In the ball example above this would mean the user could change the
position of a ball without affecting its velocity.

## Functions
Similar to values, we change functions only if their initial value has changed.
This may at first seem counterintuitive and unnecessary but it is feasible in
Python to change a function at runtime, either with decorators or by
replacement (indeed *instapy* in its current form would not be possible without
such functionality), so we must ensure that we are not resetting a function that
has not changed. As it is infeasible to programmatically verify that two
functions are indeed the same we use a suitable proxy for function equivalence:
the source code for each function. The downside to this is that if the
programmer makes only minor modifications to the source of a function (e.g.
adding a blank line or comment) then the function will be reset but the
situation in which a function may be changed during runtime is rare enough that
this is not a problem.

## Objects
Objects encapsulate state, rather than storing state intrinsically. So the
update function for an object does not need to check for equivalence or do
replacement, it merely needs to update all fields of that object. To that end,
when an object is encountered it is simply added to the frontier, along with an
initial instance of its old class and an initial instance of its new class.

## Modules
Modules produce namespaces of constants, functions, and classes; as a result
they need only be recompiled as they should not store any state. It is possible
to store state in a module but is considered poor style in most cases (the
exception being when one needs a singleton) so we consider this an acceptable
limitation on the programmer.

A currently undiagnosed bug causes module reloads to not take effect until a
second update is performed. As the rest of the update mechanism is idempotent we
remedy this by simply calling the update function twice on each update. This is
obviously not an optimal solution but for the sake of correctness it suffices
for the time being. With debug printing turned off the time for an update is
negligible even in extreme cases so this is not a huge issue.

## Lists
When a list is encountered, *instapy* treats the list as containing either only
values or only objects. It determines this by classifying the first element and
presuming the rest will be the same. If the first element is a simple value then
the entire list is treated like a value and updated as described in the value
section. If the first element is an object, however, then the list is zipped
with its old and new initial versions and each tuple in the new list is added to
the frontier. The zip used will produce a list as long as the longest of the
lists given to it, inserting `None` values where the shorter lists have ended.

This approach incurs downsides but was selected as a tradeoff, more details on
the tradeoff are given in the next chapter.

[^1]: Also known as a state transformation function or a state mapping
[^2]: Some systems will perform the transformation lazily but they will at some
      point "stop the world" in order to insert thunks.
