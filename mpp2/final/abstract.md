When writing games, programmers are often exploring potential behaviours to
evaluate them for how fun they are or how they feel to play. This involves
making small changes to the source code and observing its effect on gameplay.
With current systems for running programs the programmers must perform a
re-running step (e.g. compile and run) in order to see the changes they have
made. An existing system was extended to work on object oriented programs where
previously it had only run on procedural programs.
