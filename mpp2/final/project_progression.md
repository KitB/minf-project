In this chapter we describe the work carried out over the course of the
project in roughly chronological order.

# Recompile mechanism
Python's default module importing mechanism automatically inserts the module
into the `sys.modules` dictionary with the module's canonical name as its key.
At the beginning of the year *instapy* abused the Python import mechanism by
forcing the canonical name to change between versions, allowing multiple version
of the module to be imported. Unfortunately this cluttered `sys.modules` and
required a unique id generator to replace the canonical name. It did this as
Python intentionally makes it hard in almost all cases to prevent a loaded
module from entering sys.modules but surprisingly easy to change the canonical
name used. Early in this project this mechanism was replaced with a poorly
documented syntactic construct that allows one to execute a string of Python
code with a provided dictionary as "context" combining this with the `imp`
module's `new_module` function, which produces an empty module object, allowed
us to load module objects without ever touching `sys.modules`.

# Packages
Python provides no hooks into the mechanism by which it finds the corresponding
file for a given module. Because of this *instapy* was forced to reimplement the
mechanism. In its earlier versions this consisted only of adding ".py" to the
end of the module name. The mechanism is unfortunately not as simple as
replacing '.'s with '/'s in the fully qualified module name as the modules may
not be loaded from the current directory and if a package is imported one must
find the `__init__.py` file within that package's directory.

*instapy* was extended to perform this traversal and detect when packages were
being imported. Additionally it now detects when the imported module is not
updatable (because it is written in C) and does not attempt to do so.

During the writing of thie report it was discovered that the package import
implementation has suffered a regression and currently does not work for deeper
imports.


# User interface
The mechanisms for interacting with *instapy* were improved throughout the year;
where initially the only interaction possible was saving a file to trigger an
update. During this project a GUI was added and improved over various
iterations.

## Adding the GUI
After comparing GTK and Qt as potential toolkits for the GUI, Qt was settled
upon for having a more pleasant UI designing tool and generating almost all of
the boilerplate automatically. An initial attempt was made using Qt that simple
added a new thread to *instapy* that would run the GUI. Unfortunately Qt (and,
it seems, most UI libraries) will not allow itself (or themselves) to be run on
a non-main thread. Fixing this required a significant restructuring of
*instapy*'s execution handling code and prevented the integrated debugger from
running.

## Extracting the GUI
To remedy this and allow for a return to the simpler execution code the GUI was
extracted into an entirely separate process with its own executable. The *instapy*
process was then stripped down to only contain the updating functionality. This
meant that the file change detection mechanism was also extracted into the GUI
process. The updater process then exposed the ability to trigger an update as an
RPC interface using Python's SimpleXMLRPCServer module.

## Adding pause and restart
Later in the project the GUI was extended to allow pausing and restarting
execution of the game. This was made relatively easy by three earlier decisions.

Firstly the requirement that the programmer provide a loop body as the entry
point for execution allowed *instapy* to simply cease running that function in
order to pause execution of the game. This also means it can continue to perform
updates while the game is paused. This functionality will be useful if the
programmer wishes to explore changes to the code from a state which will only be
available briefly.

Secondly the requirement that game state be ultimately encapsulated by an object
and that that object have no dynamically added fields meant that restarting
execution in the game required only that the game object's `__init__` method be
called.

Finally the use of SimpleXMLRPCServer meant that adding new remotely available
methods (toggle pause and restart execution) required very little effort. If
plain sockets had been used then adding these methods would have required
inventing a protocol and producing encoding and decoding methods for this
protocol.

# Objects
At the beginning of the year *instapy* provided only state maintenance
capabilities for programs written in a procedural style. The ability to maintain
state encapsulated by objects was added gradually throughout the year.

## Traversing the object graph
![A potential object graph for the asteroids example](figures/object_graph_diagram.pdf)

In figure 5.1 we see a typical object graph for a small game, in this case the
asteroids game seen in chapter 3[^1]. We see in this diagram that the object
graph consists, in Python, of the links between objects and the values stored in
those objects. The blue boxes represent objects while the grey boxes represent
lists of objects.

The first barrier to maintaining state on objects is to acquire references to
every object in the system. Three prominent methods were considered for this
function:

1. Use the garbage collector's list of everything
2. Traverse the pointers between objects like a graph search
3. Use a custom root object injector (see below)

Though the first method is significantly easier it leaves us bereft of
contextual information provided by the location of an object within the graph.
This information was later proven crucial in acquiring equivalent objects from
different versions of the program.

Early in the project a module was produced that allowed *instapy* to inject a base
class to all classes inheriting from `object`. This base class kept a single
unified list of all objects. The advantage of using this module over the garbage
collector's list was that it was able to capture only a portion of the code
using a context manager. This meant that *instapy* would not attempt to update its
own objects. Unfortunately it suffered from the same lack of context as the
garbage collector list and required careful use of Python's weakref module to
prevent *all objects* from being leaked. Weak reference objects also only
support a subset of all possible values to be referenced \cite{Weakref},
placing further restrictions on the programmer.

Eventually it was decided that *instapy* would traverse the object graph. This
graph traversal mechanism went through a few versions before settling upon the
final version in use now:

#### Depth first search
Initially the traversal mechanism would immediately follow any object pointers
found and recursively perform the update function on them. This could cause a
stack overflow in the case of very large graphs but more importantly it would
get caught in loops in the graph. Loops existed even in relatively simple games
such as the asteroids example given in chapter 3.

#### Frontier as a set
To avoid the recursion problems with depth first search and as an initial
attempt to deal with loops, *instapy* was updated to use a set which objects were
added to and popped from. As a side effect this resulted in effectively random
traversal order as the set offers no guarantees on the ordering of objects
within it.

Unfortunately this implementation could be caught out if the programmer defined
an object with both hash and equality methods that could identify two different
objects as being the same (a perfectly sane thing to do).

#### Frontier as a custom set
To remedy this we implemented a set replacement that used object identity (by
memory location) rather than equality for duplicate detection. This meant that
the set would detect duplicates regardless of the definition of the object.

Unfortunately this set still allowed looping behaviour as it only ensured that
the object would not be added to the frontier if it was not currently in the
frontier, rather than if it had ever been on it.

#### Frontier and memory
To remedy this an additional set was added to the frontier that checked whether
an object had *ever* been on the frontier. A new frontier was then created every
time an update was started. This resulted in correct object graph traversal
behaviour.

## The initial object update function
As graph traversal was being added, so was the ability to maintain state across
program versions when that state was encapsulated by objects. In its first
iterations this resulted in two object update methods; one for the root game
object, and one for all other objects. They each worked in essentially the same
way: they iterated through the fields of the *new* object, checking the type of
each field, comparing it with the equivalent field in the old object, and
updating the value in the current object.

As the object update mechanism ultimately traverses down to values (leaves)
which need new and old versions in order to be updated, it is necessary that
the update function have access not only to an object in the program, but also
a corresponding object from the new version of the code, and a corresponding
object from a pristine version of the old code.

The means by which the corresponding new and old objects for a current object
were acquired was a result of the graph traversal mechanism. Each update works
with three object graphs: one that is given as input, one that is created by
creating a new game object from the old code, and one that is created by
creating a new game object from the new code. In essence these graphs are then
zipped together much like one would zip a list. Each object is linked with its
corresponding old and new objects by their location within the object graph.

## Unifying the update functions
The update functions were originally separate for a number of reasons, including
the architecture of the code and the nature of the problem, but primarily
because the game object was not initialised in the same way as other objects.
At the beginning of this project *instapy* required the game object to have an
`init` method rather than the standard Python `__init__` method. This was
essential as it allowed for the implementation of the `init_once` method without
delving into metaclass programming. *instapy* needed a way to ensure that the
`__init_once__` method (as it was eventually renamed) ran only once but also ran
before the object's `__init__` method. Normally to intercept class creation
before `__init__` one would override the `__new__` method but this did not allow
for modifying the arguments passed in to a class as we desired. Eventually a
metaclass was produced that implemented the desired functionality.

With the separate initialisation mechanisms unified, the unification of the
update mechanisms proved a relatively simple process.

## Lists
The update mechanism originally treated lists as atomic values no matter their
contents. This not only caused a full reset of all state stored in objects
within a list (or within their children!) but broke any pointers back to earlier
points in the graph. This was as the current list would be replaced entirely
with the new list whose objects all held pointers within the new object graph,
rather than the current object graph.

Lists presents one significant conceptual problem for an automatic update:

#### Inter-list object correspondence
Identifying the correspondence between objects in old, current, and new versions
of a list is not (feasibly) analytically solvable in the general case. Imagine
the programmer has completely replaced every object in the list with a different
object, how do we detect this? It may be possible to produce a system that finds
a correct correspondence often or even the majority of the time but such a
system would likely have taken half again the length of this project.

To work around this we decided to impose constraints on how the programmer could
use lists. The constraints *can* be removed if the programmer provides a custom
update method.

*instapy* can maintain state between versions of a list if:

##### The list contains either only objects or only values
Because objects and values in lists are handled differently it is not currently
possible to mix these types in lists. This could be handled by enforcing the
same constraint on mixed lists as is enforced on object lists but time on the
project ran out.

##### Lists containing objects may not change size or be reordered
This is the important constraint; this allows *instapy* to easily find a
correspondence between objects in a list as they will be at the same index.
Though adding the ability to extend or shrink the list without changing the
index of any elements (i.e. adding or removing from the end of the list) would
certainly be within scope there was not enough time to add this ability.

Additionally if the list contains only values then the list will be updated as a
unit, just like normal values. And *instapy* will still traverse every object list
and update the methods of each entry in the list, meaning changes to behaviour
will take effect but there will be no state mapping.

[^1]: The actual code for this game does not quite follow this graph but a
      previous implementation did.
