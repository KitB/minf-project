This project primarily draws on two fields for inspiration: dynamic software
updating (DSU, occasionally on-line software updating) and livecoding; there
have also been a few projects attempting to implement an instant feedback
mechanism like that demonstrated in Bret Victor's video that inspired this
project\cite{Inventing}.

# Inventing on Principle
Work on *instapy* was inspired by a video presented by Bret Victor in 2012. In
this video Victor demonstrated a number of examples of immediate feedback in
various domains. This project focusses on providing a form of immediate feedback
for video games so we focus here on the video game example.

The game example showcases a simple platforming game in which a character can
run, jump, bounce on things, etc. The code for this game is displayed alongside
it as seen in figure 2.1.

![The game environment in \cite{Inventing}](figures/victor_screenshot.pdf)

Within this environment Victor is able to change various values and behaviours
to, for example, make a turtle walk and then change its speed, all without
affecting other portions of the game. He then shows that when the player
character (PC) jumps on the turtle the PC is given negative $y$ velocity. He is
able to change this velocity value with a slider that appears on top of the
code, so he can adjust it intuitively.

He wishes to find a value for this negative $y$ velocity such that the PC lands
within a very precise range on a platform. He deems the trial and error approach
unacceptable, even with instant reloading, and shows the ability to pause the
game after recording a series of inputs and moving the game back and forth
through time, replaying these inputs. When he changes the bounce velocity he can
move the player back and cause the bounce to occur again and move forward to see
if the PC reaches the place he wants them to. Again he deems this unacceptable.

Finally he demonstrates an onion skin representation of the future and past of
the PC as seen in figure 2.2. This allows him to change the value of a variable
and immediately see the future behaviour of the system.

![The onion skin demonstration in \cite{Inventing}](figures/victor_onion.pdf)

Unfortunately none of the examples demonstrated in this system have been
released for the public and it may be the case that the examples were only
written to work for the exact changes made.

# Dynamic Software Updating
DSU is a mechanism used by administrators of servers or other long-running
processes that may need to have their behaviour modified whilst they are
running. It places no emphasis on performing these updates *while* the code is
being written but rather presumes that the programmer will prepare an entire
version of the software between updates. This may even include requiring the
programmer to provide functions that will perform the state transformation step.

A successful implementation of this project will require a DSU mechanism as its
primary component.

## Gupta et al 1996
In \cite{Gupta96} the authors describe a simple formalism for DSU mechanisms on
procedural languages and provide a definition for what constitutes a valid
update to a running program. They then mention object oriented updates but
detail is left out in favour of citing the much larger PhD thesis from which
this paper appears to be distilled.

The formalism presented defines the following components for an update:

#### The Process P
Which itself consists of a program $\Pi$ and a state $s$, the process is a
concrete, executing program. The program $\Pi$ is a sequence of statements or
functions while the state $s$ includes the program counter, any variable values,
and the execution stack in the case of procedural languages.

#### The update
Requires a new program $\Pi'$ and a state transformation function $S$ such that
$S(s)\rightarrow s'$. The update function is to be provided by the programmer.

The authors define an update $(\Pi, s, \Pi', S)$ to be valid iff the new process
$(\Pi', s')$ will reach a reachable state of $\Pi'$ within a finite number of
steps, where a reachable state is one that could be reached by the program if it
had started execution in its initial state. They then show that this problem is,
in general, undecidable. It may, however, be decided if the state transformation
function $S$ is constructed from a limited set of primitives. In general a
transformation function can be deemed valid if it directly produces $s'$ such
that $s'$ is a reachable state of $\Pi'$. Alternatively it can produce a valid
update if the new program $\Pi'$ will eventually redefine all affected variables
regardless of their value (feasible in the case of, say, a www server).

## Gupta 1994
Gupta's PhD Thesis \cite{Gupta94} describes the above formalism with varying
definitions of a program and state for commonly used programming paradigms. As
this project undertakes to add object oriented update functionality to an
existing system we focus on the object oriented section of the thesis.

Gupta describes a graph based representation of the state of a program in which
vertices either represent objects or base values (e.g. simple types such as
integers) and the directed edges represent pointers between objects.
Additionally each stack frame in the current stack of a state is modelled as a
vertex with a pointer to the previous stack frame and all local variables as
well as a single pointer to the object whose method the stack frame is
executing.

Gupta assumes that the state transformation function will be provided by the
programmer so but provides an effective framework for performing provided
"restructuring mappings" that is too large to detail here.

## Buckley et al 2005
\cite{Buckley} attempts to produce a formal taxonomy for classifying mechisms,
systems, and formalisms for producing changing software behaviour. The majority
of work in this paper goes to classifying non-dynamic systems, including tools
such as version control systems.

## Hicks 2001
Hicks' PhD Thesis \cite{DSU} describes an implementation of a DSU system and a
compiled language designed for the task of DSU. Its major focusses are on how to
provide DSU for compiled languages with little performance degredation during
normal running.

*instapy* is focussed on an interpreted language that already has the overhead
of being dynamic so was unable to make use of this paper's major contributions.

## Erlang
Erlang's OTP libraries allow a programmer to perform a code update on a running
system. They rely somewhat on Erlang's distributed, message passing, model of
computation and require the programmer to specify state stransformation
functions.

*instapy* focusses on programs in a procedural or object oriented style, rather
than message passing and Erlang's update mechanism was designed for large,
long-running processes on production servers, so we were unable to use many of
the ideas from Erlang in *instapy*.

## Livecoding Python Package
The livecoding[^2] package for Python (\cite{LivecodingPy}) provided a code
reloading mechanism for older versions of Python that would perform some
rudimentary state transformation of simple values and fields. It relied on a
custom module system in which the programmer would have to register their module
to be updated in order to reduce update time and to reduce the number of files
that must be checked for updates. This reliance on the programmer to tag certain
modules as updating reduces the ease of use of the system; the most preferable
user experience would be to simply replace the python runtime with an augmented
runtime and continue as normal.

Interestingly this package would automatically perform unit test suites between
updates, allowing the programmer to include some guarantees of functionality
between program versions. Additionally it included methods that would detect and
delete objects that were leaked during the update process.

Of the libraries and papers surveyed this is the only one that attempts to infer
a state transformation function in an object oriented context or beyond the
simplest transformations.

## Pymoult
The more recent Pymoult package (\cite{Pymoult}) for Python provides a framework
for building DSU systems for the Python programming language; it defines a
number of update strategies such as eager and lazy updates which will either
update the object immediately or wrap the object in a proxy that will convert
the object when requested. Similar to \cite{Gupta96} this library presumes state
transformation functions will be provided by the programmer.

Had Pymoult been available and noticed earlier it may have provided a
useful foundation for *instapy*, however it is in a pre-release stage of
development and was only begun as a project after implementation on *instapy* had
begun.

# Livecoding
Livecoding is a performance art in which a programmer or artist writes a program
that produces graphical and/or audio output in front of an audience. A common
feature of livecoding performances is that the output will be uninterrupted as
the programmer changes the behaviour. This allows the programmer to evolve a
performance from a small starting point without the overhead of the compile and
run cycle.

Livecoding systems are very specific systems which often provide a limited API
with which to produce output and enforce constraints upon the programmer that
would be unacceptable in normal programming environments. For example they may
require that dynamic systems be based solely upon a time variable rather than
maintaining state or they may include their own editor component that will lack
capabilities expected for a longer-term programming project. This enforced
workflow opposes such systems to the task of writing software systems that may
need to be maintained at a later date as they are only designed for ephemeral
programs which could well be deleted immediately after a performance ends.

A livecoding system may even require that the programmer know details of the
mechanism by which the system updates code as it can typically be considered a
language in its own right.

As a result these systems see very little uptake outside of artistic and
performance programming.

## Fluxus
![A screenshot of fluxus in action, taken from \cite{Fluxus}](figures/fluxus.pdf)

Fluxus (\cite{Fluxus}) is a livecoding environment designed for writing games,
it provides a livecoding mechanism only insofar as it allows the user to press
F5 to restart the running script and, optionally, leave the objects from the old
script in place. Additionally it combines the editor and game into a single
surface, overlaying the game with the source code for the game in order to
support the livecoding performance.

## e15:oGFx
The e15:oGFx (\cite{E15}) project allows the programmer to draw a 2d surface
each frame and uses this surface as the cross section of a 3d object as seen in
figure 2.4. The lower-right section shows the 2d surface
while the larger section shows the 3d object generated over time.

Programs are run in e15:oGFx via the embedded Python interpreter and editor.
The update mechanism provided will update the methods of an object (thereby
changing future behaviour) but will not perform any state transformation[^1].

![e15:oGFx ball example](figures/e15ogfx.pdf)

# Instant feedback implementations
The idea of an immediate feedback system has entered the zeitgeist recently,
possibly as a direct result of \cite{Inventing}. livecoding.io and codebook are
both direct responses to the video but [Light Table](#light-table) appeared soon
after the video, influenced by it but with a different focus.

## livecoding.io and codebook
Both projects \cite{Livecoding}\cite{Codebook} implement a javascript editor
that provides instant feedback. They also both implement the value scrubbing
feature demonstrated in the video (with which one can drag a numerical value
with the mouse to increase or decrease it). Neither project appears to implement
any kind of state transition function as is necessary for producing a game.

## Light Table
Light Table is an IDE project started at least partly in response to the
original video. It promised to offer similar reloading functionality to that
displayed in the video, with results displayed alongside code. As *instapy* will
be working with Python only the Python related abilities of Light Table were
evaluated.

Light table offers simple evaluation of a statement under the cursor, it will
display this result in line. Evaluating an expression then changing the
expression does not produce changes in the displayed result, requiring that the
programmer initiate the evaluation again using a key command. It appears to be
the case that if a programmer evaluates an expression then changes a dependency
of that expression (for example if the programmer has an expression that calls a
function and makes a change in the function) then the programmer must first
re-evaluate the dependency, then the expression itself in order to see the
changes.

[^1]: A conclusion I came to by inspecting the source code as the project runs
      only on Mac OS X and has been untouched in the alpha stage since 2008.
[^2]: Notably *not* a project explicitly in the livecoding tradition mentioned
      later, though it could well be used in such a fashion.
