% Programming as an Interactive Experience
% MInf Project Phase One
% Kit Barnes - s0905642

> ## Abstract
> Current programming workflows require that the programmer be interrupted when
> they wish to run their code. A system was produced that eliminates this
> interruption for a useful subset of a popular programming language. This
> system will continue to be improved to allow a larger subset of the language
> in future.

# Introduction
This is a progress report on a project that aims to produce a system allowing
live updating of a running, two-dimensional, game written in the Python
programming language. This report draws upon a planning report for this project
and evaluates the work that has been carried out since the plan was written.

# Motivation
A typical programming workflow consists of the programmer:

1. Making some changes to the code
2. Compiling that code
3. Running the code to see the changes

In interpreted languages we skip step two but we must still run our changed code
to see how the output has changed. This presents three problems:

1.  This is an interruption that removes the programmer from his code, however
    briefly. It is well known that interruptions have strong negative effects on
    programmer productivity; typically causing the programmer to cease work for
    multiple minutes[@Peopleware].
2.  In the case of a long running and highly stateful process such as a server
    for a computer game it is desirable that we avoid any interruption to the
    service provided by that server. As a result the process of compilation and
    re-running the server to perform an update is something we wish to eliminate;
    instead it is desirable to apply the update to the running process with no
    discontinuity of service.
3.  In creative programming works -- such as computer games (or indeed any
    programming according to some) -- this interruption stints the exploratory,
    creative work that the programmer is doing. Compare for instance the classic
    image manipulation tools ImageMagick and Photoshop:
     - In ImageMagick you start with an image and write a command that will in
       some way manipulate it. This requires that you envisage the change
       yourself.
     - In Photoshop most of the available tools are applied onto the image in
       real-time. This allows the user to "ask the computer" what a change will
       look like rather than having to imagine it themselves.

    In the context of programming computer games we can imagine a programmer
    playing with the value and behaviour of gravity to find a value which
    "feels right" -- an important property for a game. Or even a programmer
    exploring different behaviours for player motion to discover fun mechanisms.

    All of these activities are much harder than necessary due to the compile
    and run cycle.

For each of these problems I have identified potential solutions:

1. To prevent the programmer from having to take his mind off of the code at any
   point we must automatically run the code as seamlessly as possible. An
   example of this would be a simple Eclipse plugin that compiles and runs the
   current code whenever the user saves (or possibly when they stop typing).
2. To allow a long-running and highly stateful process to be updated we can draw
   upon the work of Hicks et al in the Dynamic Software Updating (DSU) discipline
   [@DSU].
3. Combining the above two to produce a system that will automatically inject
   new behaviours and values into an already running program requiring minimal
   interaction from the programmer. An example of this would be a game engine
   that listens for changes to the source files that comprise it and updates
   using DSU techniques when it detects a change. This would result in a
   workflow where the user simply writes code and can immediately see the
   changes.

This third task is what this project undertakes to complete.

# Work so far
A system has been produced that performs much of the task defined above and is
indeed a usable system for many tasks. Figure 1 displays the system in use with
the demo program from appendix A loaded and running.

This system allows the programmer to write code in any editor and their changes
will be read in from the filesystem and applied to the running program when they
save the file. This workflow is slightly removed from that described above
(requiring the programmer to save to initiate updates) as doing otherwise
requires either a plugin for an existing text editor or a dedicated text editor.

![The system in action](figures/screenshot.png)

## What the system is capable of
The system is currently capable of many useful transformations to a running
system. It can arbitrarily transform the *values* of data stored in a
non-object-oriented style. It can also accept changes to the structure of the
code (such as extracting sets of functions into a distinct module) and the
behaviour of functions (unsurprising as functions in Python are data). The
system cannot currently transform the structure of data storage, nor can it
correctly transform systems which define complex data structures.

The system enforces a structure upon the programs written using it; all programs
must begin execution with an object that looks like the following (taken from
Appendix A):

~~~~{.python .numberLines}
class Game(instapy.Looper):
    def init_once(self):
        pygame.init()
        ...

    def init(self):
        self.radius = 50
        ...

    def loop_body(self):
        ...
        self.update_ball()
        self.draw()
~~~~

Where the functions `init`, `init_once`, and `loop_body` must all exist and
act similarly.

The following examples are all modifications to the source code provided in
appendix A. The program this source code produces shows a ball bouncing around
in a small box and allows the user to control the ball with `w`, `a`, `s`, and
`d`. The ball is influenced by both gravity and pseudo-friction (a simple
damping factor; not a realistic model of friction) as well as having imperfect
elasticity (though again not realistically modelled -- no momentum is involved,
only velocity).

### Reinitialisation
Any properties that are added to the object in its `init` method can be safely
changed *without affecting the values of unchanged properties*. This means that
changing the radius of the ball does not place it back into its initial
position.

For instance if we change the radius of the ball

~~~~{.python .numberLines startFrom="21"}
        self.radius = 100
~~~~

and save then we will immediately see the ball increase in size:  
![Changing the radius](figures/radius.pdf)

Or if we change the initial position of the ball

~~~~{.python .numberLines startFrom="20"}
        self.ball_pos = (20, 20)
~~~~

then the ball will move to the new position *whilst preserving its velocity and
radius etc*:  
![Changing the position](figures/position.pdf)

Or if we change the velocity of the ball after it has come to rest

~~~~{.python .numberLines startFrom="23"}
        self.ball_velocity = (0, -320)
~~~~

then it will appear to jump:  
![Changing the velocity](figures/velocity.pdf)

We can change the value of gravity (even making it negative):

~~~~{.python .numberLines startFrom="26"}
        self.gravity = -900
~~~~

and the ball will begin to fall upwards.

![Changing gravity](figures/gravity.pdf)  
We can change elasticity:

~~~~{.python .numberLines startFrom="28"}
        self.elasticity = 0.4
~~~~

and the ball will lose more velocity when it bounces.

![Changing elasticity](figures/elasticity.pdf)&nbsp;

### Behavioural changes
We can also modify the methods of the Game class to change behaviour; for
instance if we wanted to achieve the same behaviour as inverting gravity but in
function rather than value:

~~~~{.python .numberLines startFrom="73"}
        self.ball_velocity[1] -= self.gravity * dt_seconds
~~~~

Then we will see the ball fall upwards again  
![Changing gravity in behaviour](figures/gravity.pdf)&nbsp;

Or if we wish to allow the user to change this:

~~~~{.python .numberLines startFrom="4"}
from pygame.locals import KEYDOWN, K_w, K_s, K_a, K_d, K_SPACE
~~~~

~~~~{.python .numberLines startFrom="101"}
        if keys[K_SPACE]:
            self.gravity = -self.gravity
~~~~

now the user can flip gravity by pressing the space bar.

![User changing gravity](figures/interactive_gravity.pdf)&nbsp;

Arbitrary changes to methods are supported, the only requirement being that they
have the same name.

### Structural changes
The system can accept transformations in the structure of the code. For instance
if we decide to be sensible and extract our non-game functions into their own
modules:

~~~~{.python .numberLines}
def darken((r, g, b)):
    """ Make a darker version of a colour """
    return (r / 4, g / 4, b / 4)


def invert((r, g, b)):
    return (255 - r, 255 - g, 255 - b)
~~~~

~~~~{.python .numberLines startFrom="8"}
from color import darken, invert
~~~~

and remove the two colour functions from the main source.

This transformation to the code will produce *no change* in the behaviour while
the code is still running, as one would hope. But if we now add a line in
`color.py`:

~~~~{.python .numberLines startFrom="2"}
    """ Make a darker version of a colour """
    print "Darkening"
    return (r / 4, g / 4, b / 4)
~~~~

Then the program will begin printing "Darkening", demonstrating that the new
file is being used.

Similar changes can easily be made for the vector functions.

### Larger structural changes
We can even begin from a minimal piece of code that does very little and make
large changes of all types. The sequence of versions of a single file given in
appendix B demonstrate this; and the behaviour of this sequence appears as
follows (and is demonstrated graphically in figure 2):

![Larger structural changes](figures/incremental.pdf)

1. First we are presented with a blank canvas; this code is the minimal example
   that can be   transformed into the following examples. It is hoped that a future
   version of the system  will be capable of starting   from nothing and adding
   this code in whilst running.

2. Next a stationary ball is drawn at a rate of 60 frames per second.

3. Then this ball is set in motion. Its motion is dampened by a constant ratio to
   prevent it from rapidly exiting the screen.

4. The ball will begin to fall due to gravity but will exit the screen as no ground
   exists.

5. Now the ball will bounce off of the edges of the screen, losing velocity each
   time due to non-perfect elasticity.

6. Finally the ball can now be controlled by the user.

## How I did this
If the system were to force to rigid a structure upon the programmer then it
would likely not be applicable to the program that is being written; on the
other hand it was necessary to limit the allowed code at least somewhat as
arbitrary code transformation is outside of the scope of this project. In aid of
this it was decided that the system be limited to allow only software involving
a main loop function that could be called externally. Additionally software is
required to have its point of entry and all state stored in a root object.

To allow this limitation to be sensible it was necessary to limit the domain of
the project and that of two-dimensional games programming was selected. This
limiting of the domain was successful and the produced system can reasonably be
used to produce games, the only limitations being that the game have a "main
loop", some initialization, and optionally some special initialization that will
only be performed once. This special initialization exists to allow the setup of
components of the project that:

a. Are unlikely to be changed and
b. Would cause errors if they were to be initialized twice.

The system makes use of Python's dynamic nature, allowing it to:

a. Easily and quickly recompile modules to allow access to the new behaviours
   and
b. Easily replace all aspects of any object at run-time. As everything in Python
   is an object (including classes) this allows us full control of behaviour at
   run time.

The `play` function works as such:

~~~~{.numberLines}
Initialize the game object
Loop until done:
    if we have been notified of a changed file:
        run the update process
    run the game object's loop function
~~~~

where the user writes "the game object's loop function". This requirement --
that the looping itself be performed by the system, rather than the game -- is a
crucial property of this system that allows the system to perform updates
between frames, avoiding race conditions and giving the system execution time. A
considered alternative mechanism was to augment all user functions to perform
the check themselves.

The update mechanism itself is performed as such:

~~~~{.numberLines}
Acquire a new version of the module that holds the main game class
Initialize a new game object using the new code
Initialize a new game object using the old code
Update any properties that differ between these two objects
Update the methods of the game object from the new object
Replace all loaded modules with updated versions
~~~~

Both of these functions are augmented by a separate thread which watches the
source files for changes and notifies the play thread when it finds that one has
changed. Both the `play` function and `update` function are provided by the
system but the user would be expected to write their own `main` function for a
production version of the game they write (one which does not make use of this
system).

A full source code listing is provided in appendix C

# Work plan
There are two distinct paths of remaining work: that of enlarging the set of
acceptable code and that of improving the interface that the programmer will
work with.

## Enlarging the set of acceptable code
To improve the utility and experience of using this project it is necessary that
it limit the programmer as little as possible. In aid of this the project will
support as many language features as it can; most notably lacking in the current
implementation is support for objects. Any sizeable project in Python is likely
to use objects if only for encapsulation so it is imperative that they be
supported.

Supporting objects fully can be split into three stages:

1. Traversing and updating the object graph
2. Providing mechanisms to allow transformations between object versions
3. Inferring the transformations to be used between object versions

The first is important in maintaining the full state of the system as if we only
transform the first level of objects in the digraph of objects that contain
references to each other then we will discard much of the state of the system as
we inevitably reproduce the rest of the graph. The difficulty of this particular
task was unexpected; updating an object requires that we update all referenced
objects, traversing the object graph (which may include cycles). This is a
qualitatively different task than the current update mechanism implements.

The second allows the programmer to change the structure of the stored data
rather than just the values and the third makes the second easier on the
programmer.

## Improving the interface
As it stands the system allows the programmer to work with any text editor or
IDE they wish to use; this is a great boon in terms of freedom of tools but does
not allow for more fine-grained control of the system (such as preventing
updates or pausing the running code). Additionally the user is required to save
to initiate the update process; it is desirable that the process happen
automatically when the user ceases typing, or even at every keystroke.

To remedy this new user interfaces will be produced that provide this greater
degree of control. First the functions to allow such control will be added to
the project, then a standalone GUI will be produced that makes use of the
functions (as much as it can if the functions require reference to lines of
code), then finally a plugin will be produced for an existing text editor that
makes use of these functions. A plugin was selected over producing a text editor
because producing a useful text editor is outside of the scope of this project.

# Time plan
These two paths of improvement will be undertaken simultaneously as they are
largely orthogonal to each other.

----------------------------------------------------------------------------------------
Month       Enlarging the acceptable set   Improving the interaction             Writeup
----------  -----------------------------  ---------------------------  ----------------
September                                  Add functions for finer and
                                           interactive control to the
                                           system itself.

October     Begin work on object updating  Produce a standalone GUI

November    Finish work on object
            updating, begin structural
            transformation work

December    Continue transformation work   Produce a text editor        Write a draft of
                                           plugin                       the second
                                                                        interrim
                                                                        progress report

January     Finish work on
            transformations, begin work
            on transformation inference

February    Finish work on inference                                    Start final
                                                                        report

March                                                                   Continue
                                                                        work on
                                                                        final
                                                                        report

April                                                                   Hand in
                                                                        final report
----------------------------------------------------------------------------------------

# Progress
In the proposal for this project[@Proposal] it was suggested that the project
be completed incrementally with milestones as follows:

1. An API stub that will simply run programs without transforming them.
2. An API which provides real time update. Starting with top-level variables and
   working through the levels of language usage up to transforming multiple
   modules.
3. An interface that makes use of this API.
4. A refinement of this library allowing for state transformation where
   transformation functions are provided by the programmer. This will include
   transforming classes using programmer provided transformation functions.
5. A suite of prebuilt state transformation functions for common cases that can
   be automatically inferred. Importantly providing inference of class
   transformations.

This plan has been followed, with the current system at stage three of this
plan. Further elements have been added to the plan to compensate for the
difficulty of object transformation and to better specify the improved interface
later suggested in the proposal.

An API for the system was also described in the proposal:

* `watch`: Begin bookkeeping on a large unit of code (in Python this would be a
           module, in Java a class).
* `start`: Begin program execution. Possibly taking as a parameter a function
           at which to begin execution.
* `stop`: Cease program execution.
* `update`: Taking the new source code as input, modify the running program to
            reflect it.
* `pause`: Cease program execution in such a way that it can be resumed from
           its current state.

This API has been largely ignored in favour of generating a working system,
though it is still my intent to follow a similar API in future. The system
currently only has the `start` and `update` functions implemented, and the
`update` function takes no source code as input but rather re-reads the original
source file.

In future versions the API will likely follow that described above but without
the `watch` function as no bookkeeping seems to be necessary.

# Evaluation
A fully working system has been produced that can apply changes from a useful
subset of the Python programming language to the corresponding running
processes.

The project is on track to produce a system that can both accept a larger subset
of Python and provide more mechanisms of interaction for programmers to use.

The success of the system is largely due to the limitation of a main loop that
is imposed upon the programmer. This idea was borrowed in part from projects
such as Fluxus and e15:oGFx -- both projects described and evaluated in the
proposal[@Proposal]. On the other hand this limitation may be avoidable with
careful use of Python's decorators.

In the proposal it was planned that the system would be at stage four (from
Progress, above) by this point. During implementation it became apparent that
transformations involving objects (specifically references) required a great
deal more work than previously expected. As a result object transformations have
been deferred until next year.

\newpage

# Appendix A -- Sample Program
~~~~{.python .numberLines}
import pygame
import instapy

from pygame.locals import KEYDOWN, K_w, K_s, K_a, K_d

from math import sqrt, cos, sin
import time


class Game(instapy.Looper):
    def init_once(self):
        pygame.init()
        self.screen = pygame.display.set_mode((640, 640))
        self.clock = pygame.time.Clock()

    def init(self):
        self.purple = (128, 0, 128)
        self.bg = (0, 0, 0)

        self.ball_pos = (320, 320)
        self.radius = 50

        self.ball_velocity = (20, 20)
        self.damping = 0.8

        self.gravity = 900

        self.elasticity = 0.7

        # Player control speeds
        ## roughly equal to gravity seems to work nicely
        self.jump = 900
        self.move = 900

    def loop_body(self):
        t = time.time()

        self.purple = ((cos(t) * 128) + 127,
                       (sin(t) * 128) + 127,
                       (-cos(t) * 128) + 127)
        self.bg = darken(invert(self.purple))

        self.handle_events()
        self.update_ball()
        self.draw()

    def draw(self):
        # Redraw the background
        self.screen.fill(self.bg)

        # Draw the foreground
        ## First we need drawable coordinates because we want subpixel precision
        ## when calculating but we can only draw in integer pixel coordinates
        pos = [int(round(n)) for n in self.ball_pos]
        pygame.draw.circle(self.screen, self.purple, pos, self.radius)
        pygame.draw.circle(
            self.screen, darken(self.purple), pos, self.radius - 5)

        # Switch to the new buffer in video memory
        pygame.display.flip()

    def update_ball(self):
        dt = self.clock.tick(60)
        dt_seconds = dt / 1000.0

        self.ball_pos = [a + (b * dt_seconds)
                         for (a, b)
                         in zip(self.ball_pos, self.ball_velocity)]
        self.ball_velocity = [n * (1 - (self.damping * dt_seconds))
                              for n
                              in self.ball_velocity]

        self.ball_velocity[1] += self.gravity * dt_seconds

        if self.ball_pos[1] > 640 - self.radius:
            self.ball_pos[1] = 640 - self.radius
            self.vertical_bounce()
        elif self.ball_pos[1] < self.radius:
            self.ball_pos[1] = self.radius
            self.vertical_bounce()

        if self.ball_pos[0] > 640 - self.radius:
            self.ball_pos[0] = 640 - self.radius
            self.horizontal_bounce()
        elif self.ball_pos[0] < self.radius:
            self.ball_pos[0] = self.radius
            self.horizontal_bounce()

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                keys = pygame.key.get_pressed()
                if keys[K_w]:
                    self.ball_velocity[1] -= self.jump
                if keys[K_s]:
                    self.ball_velocity[1] += self.jump
                if keys[K_a]:
                    self.ball_velocity[0] -= self.move
                if keys[K_d]:
                    self.ball_velocity[0] += self.move

    def vertical_bounce(self):
            self.ball_velocity[1] = self.ball_velocity[1] * -self.elasticity
            if abs(self.ball_velocity[1]) < 60:
                self.ball_velocity[1] = 0

    def horizontal_bounce(self):
            self.ball_velocity[0] = self.ball_velocity[0] * -self.elasticity
            if abs(self.ball_velocity[0]) < 60:
                self.ball_velocity[0] = 0


def darken((r, g, b)):
    """ Make a darker version of a colour """
    return (r / 4, g / 4, b / 4)


def vector_distance((x1, y1), (x2, y2)):
    return sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


def vector_subtract((x1, y1), (x2, y2), factor=1):
    return ((x1 - x2) * factor, (y1 - y2) * factor)


def invert((r, g, b)):
    return (255 - r, 255 - g, 255 - b)
~~~~

\newpage

# Appendix B -- Incrementally producing the sample program
## Version One
This program does only the things that cannot be performed more than once --
setting up the drawing area and game clock.

~~~~{.python .numberLines}
import pygame
import instapy

class Game(instapy.Looper):
    def init_once(self):
        pygame.init()
        self.screen = pygame.display.set_mode((640, 640))
        self.clock = pygame.time.Clock()

    def init(self):
        pass

    def loop_body(self):
        pass
~~~~

## Version Two
This program starts drawing the ball at 60 frames per second

~~~~{.python .numberLines}
import pygame
import instapy

class Game(instapy.Looper):
    def init_once(self):
        pygame.init()
        self.screen = pygame.display.set_mode((640, 640))
        self.clock = pygame.time.Clock()

    def init(self):
        # We add some colours
        self.purple = (128, 0, 128)
        self.bg = (0, 0, 0)

        # And some stateful stuff
        self.ball_pos = (320, 320)
        self.radius = 50

    def loop_body(self):
        # And we make the loop body do the drawing
        self.draw()
        self.clock.tick(60)

    # We're adding a new function!
    def draw(self):
        # Redraw the background
        self.screen.fill(self.bg)

        # Draw the foreground
        pygame.draw.circle(self.screen, self.purple, self.ball_pos, self.radius)
        pygame.draw.circle(
               self.screen, darken(self.purple), self.ball_pos, self.radius - 5)

        # Switch to the new buffer in video memory
        pygame.display.flip()

def darken((r, g, b)):
    """ Make a darker version of a colour """
    return (r / 4, g / 4, b / 4)
~~~~

## Version Three
This version sets the ball in motion with a small damping factor to prevent it
from continuing on indefinitely.

~~~~{.python .numberLines}
import pygame
import instapy

class Game(instapy.Looper):
    def init_once(self):
        pygame.init()
        self.screen = pygame.display.set_mode((640, 640))
        self.clock = pygame.time.Clock()

    def init(self):
        self.purple = (128, 0, 128)
        self.bg = (0, 0, 0)

        self.ball_pos = (320, 320)
        self.radius = 50

        # Adding velocity, demonstrating statefulness
        self.ball_velocity = (20, 20)
        # And some damping so that it doesn't just move forever
        self.damping = 0.8

    def loop_body(self):
        # We have to make the ball move now
        self.update_ball()
        self.draw()
        # We no longer tick the clock here because we need the value later

    def draw(self):
        # Redraw the background
        self.screen.fill(self.bg)

        # Draw the foreground
        pos = [int(round(n)) for n in self.ball_pos]
        pygame.draw.circle(self.screen, self.purple, pos, self.radius)
        pygame.draw.circle(
               self.screen, darken(self.purple), pos, self.radius - 5)

        # Switch to the new buffer in video memory
        pygame.display.flip()

    def update_ball(self):
        # Now we tick the clock here
        dt = self.clock.tick(60)
        dt_seconds = dt / 1000.0

        # I know these lines are ridiculous but I like them anyway
        self.ball_pos = [a + (b * dt_seconds)
                         for (a, b)
                         in zip(self.ball_pos, self.ball_velocity)]
        self.ball_velocity = [n * (1 - (self.damping * dt_seconds))
                              for n
                              in self.ball_velocity]

def darken((r, g, b)):
    """ Make a darker version of a colour """
    return (r / 4, g / 4, b / 4)
~~~~

## Version Four
This version adds gravity to the ball.

~~~~{.python .numberLines}
import pygame
import instapy

class Game(instapy.Looper):
    def init_once(self):
        pygame.init()
        self.screen = pygame.display.set_mode((640, 640))
        self.clock = pygame.time.Clock()

    def init(self):
        self.purple = (128, 0, 128)
        self.bg = (0, 0, 0)

        self.ball_pos = (320, 320)
        self.radius = 50

        self.ball_velocity = (20, 20)
        self.damping = 0.8

        # Adding another physical constant
        ## We set it rather low so that the ball doesn't rapidly exit the screen
        ## because we don't have collisions yet
        self.gravity = 30

    def loop_body(self):
        self.update_ball()
        self.draw()

    def draw(self):
        # Redraw the background
        self.screen.fill(self.bg)

        # Draw the foreground
        ## First we need drawable coordinates because we want subpixel precision
        ## when calculating but we can only draw in integer pixel coordinates
        pos = [int(round(n)) for n in self.ball_pos]
        pygame.draw.circle(self.screen, self.purple, pos, self.radius)
        pygame.draw.circle(
               self.screen, darken(self.purple), pos, self.radius - 5)

        # Switch to the new buffer in video memory
        pygame.display.flip()

    def update_ball(self):
        # Now we tick the clock here
        dt = self.clock.tick(60)
        dt_seconds = dt / 1000.0

        # I know these lines are ridiculous but I like them anyway
        self.ball_pos = [a + (b * dt_seconds)
                         for (a, b)
                         in zip(self.ball_pos, self.ball_velocity)]
        self.ball_velocity = [n * (1 - (self.damping * dt_seconds))
                              for n
                              in self.ball_velocity]

        # We add this line to make velocity accelerate downwards
        self.ball_velocity[1] += self.gravity * dt_seconds

def darken((r, g, b)):
    """ Make a darker version of a colour """
    return (r / 4, g / 4, b / 4)
~~~~

## Version Five
This version makes the ball bounce off of the walls.

~~~~{.python .numberLines}
import pygame
import instapy


class Game(instapy.Looper):
    def init_once(self):
        pygame.init()
        self.screen = pygame.display.set_mode((640, 640))
        self.clock = pygame.time.Clock()

    def init(self):
        self.purple = (128, 0, 128)
        self.bg = (0, 0, 0)

        self.ball_pos = (320, 320)
        self.radius = 50

        self.ball_velocity = (20, 20)
        self.damping = 0.8

        self.gravity = 900

        # Another physical constant
        self.elasticity = 0.7

    def loop_body(self):
        self.update_ball()
        self.draw()

    def draw(self):
        # Redraw the background
        self.screen.fill(self.bg)

        # Draw the foreground
        ## First we need drawable coordinates because we want subpixel precision
        ## when calculating but we can only draw in integer pixel coordinates
        pos = [int(round(n)) for n in self.ball_pos]
        pygame.draw.circle(self.screen, self.purple, pos, self.radius)
        pygame.draw.circle(
               self.screen, darken(self.purple), pos, self.radius - 5)

        # Switch to the new buffer in video memory
        pygame.display.flip()

    def update_ball(self):
        dt = self.clock.tick(60)
        dt_seconds = dt / 1000.0

        # I know these lines are ridiculous but I like them anyway
        self.ball_pos = [a + (b * dt_seconds)
                         for (a, b)
                         in zip(self.ball_pos, self.ball_velocity)]
        self.ball_velocity = [n * (1 - (self.damping * dt_seconds))
                              for n
                              in self.ball_velocity]

        self.ball_velocity[1] += self.gravity * dt_seconds

        # Now we check the position of the ball and bounce if necessary
        if self.ball_pos[1] > 640 - self.radius:
            self.ball_pos[1] = 640 - self.radius
            self.vertical_bounce()
        elif self.ball_pos[1] < self.radius:
            self.ball_pos[1] = self.radius
            self.vertical_bounce()

        if self.ball_pos[0] > 640 - self.radius:
            self.ball_pos[0] = 640 - self.radius
            self.horizontal_bounce()
        elif self.ball_pos[0] < self.radius:
            self.ball_pos[0] = self.radius
            self.horizontal_bounce()

    def vertical_bounce(self):
            self.ball_velocity[1] = self.ball_velocity[1] * -self.elasticity
            # If we don't add this bit it never stops bouncing
            if abs(self.ball_velocity[1]) < 60:
                self.ball_velocity[1] = 0

    def horizontal_bounce(self):
            self.ball_velocity[0] = self.ball_velocity[0] * -self.elasticity
            # Because we might as well be consistent
            if abs(self.ball_velocity[0]) < 60:
                self.ball_velocity[0] = 0

def darken((r, g, b)):
    """ Make a darker version of a colour """
    return (r / 4, g / 4, b / 4)
~~~~

## Version Six
This version adds player control. The next version is given in appendix A and
adds changing colours to the ball and background.

~~~~{.python .numberLines}
import pygame
import instapy

# Adding an import!
from pygame.locals import *


class Game(instapy.Looper):
    def init_once(self):
        pygame.init()
        self.screen = pygame.display.set_mode((640, 640))
        self.clock = pygame.time.Clock()

    def init(self):
        self.purple = (128, 0, 128)
        self.bg = (0, 0, 0)

        self.ball_pos = (320, 320)
        self.radius = 50

        self.ball_velocity = (20, 20)
        self.damping = 0.8

        self.gravity = 900

        self.elasticity = 0.7

        # Player control speeds
        ## roughly equal to gravity seems to work nicely
        self.jump = 900
        self.move = 900

    def loop_body(self):
        self.handle_events()
        self.update_ball()
        self.draw()

    def draw(self):
        # Redraw the background
        self.screen.fill(self.bg)

        # Draw the foreground
        ## First we need drawable coordinates because we want subpixel precision
        ## when calculating but we can only draw in integer pixel coordinates
        pos = [int(round(n)) for n in self.ball_pos]
        pygame.draw.circle(self.screen, self.purple, pos, self.radius)
        pygame.draw.circle(
               self.screen, darken(self.purple), pos, self.radius - 5)

        # Switch to the new buffer in video memory
        pygame.display.flip()

    def update_ball(self):
        dt = self.clock.tick(60)
        dt_seconds = dt / 1000.0

        # I know these lines are ridiculous but I like them anyway
        self.ball_pos = [a + (b * dt_seconds)
                         for (a, b)
                         in zip(self.ball_pos, self.ball_velocity)]
        self.ball_velocity = [n * (1 - (self.damping * dt_seconds))
                              for n
                              in self.ball_velocity]

        self.ball_velocity[1] += self.gravity * dt_seconds

        if self.ball_pos[1] > 640 - self.radius:
            self.ball_pos[1] = 640 - self.radius
            self.vertical_bounce()
        elif self.ball_pos[1] < self.radius:
            self.ball_pos[1] = self.radius
            self.vertical_bounce()

        if self.ball_pos[0] > 640 - self.radius:
            self.ball_pos[0] = 640 - self.radius
            self.horizontal_bounce()
        elif self.ball_pos[0] < self.radius:
            self.ball_pos[0] = self.radius
            self.horizontal_bounce()

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                keys = pygame.key.get_pressed()
                if keys[K_w]:
                    self.ball_velocity[1] -= self.jump
                if keys[K_s]:
                    self.ball_velocity[1] += self.jump
                if keys[K_a]:
                    self.ball_velocity[0] -= self.move
                if keys[K_d]:
                    self.ball_velocity[0] += self.move

    def vertical_bounce(self):
            self.ball_velocity[1] = self.ball_velocity[1] * -self.elasticity
            if abs(self.ball_velocity[1]) < 60:
                self.ball_velocity[1] = 0

    def horizontal_bounce(self):
            self.ball_velocity[0] = self.ball_velocity[0] * -self.elasticity
            if abs(self.ball_velocity[0]) < 60:
                self.ball_velocity[0] = 0

def darken((r, g, b)):
    """ Make a darker version of a colour """
    return (r / 4, g / 4, b / 4)
~~~~

\newpage

# Appendix C -- Project source listing
## instapy.py
This files provides most of the project functionality, including the update
function.

~~~~{.python .numberLines}
import imp
import threading
import inspect
import types
import re
import traceback
import time


class CachedReloader(object):
    def __init__(self):
        self._n_reloads = 0
        self._d = dict()

    def new_generation(self):
        del self._d
        self._d = dict()

    def get_module(self, str_or_module):
        if isinstance(str_or_module, basestring):
            try:
                return self._d[str_or_module]
            except KeyError:
                m = self._load_module(str_or_module)
                self._d[str_or_module] = m
                return m
        elif isinstance(str_or_module, types.ModuleType):
            return self.get_module(self._get_module_name(str_or_module))

    def _load_module(self, import_str):
        self._n_reloads += 1
        suffix = "_reloaded_%d_times" % self._n_reloads
        return imp.load_module(import_str + suffix, *imp.find_module(import_str))

    def _get_module_name(self, module):
        suffix_re = re.compile("^(.*)_reloaded_[0-9]+_times$")
        m = suffix_re.match(module.__name__)
        if m:
            return m.group(1)
        else:
            return module.__name__


class Looper(object):
    """ Mostly unnecessary at the moment
        Provides default methods, gives future potential for modifying class
        behaviours """
    def init_once(self):
        pass

    def init(self):
        pass

    def loop_body(self):
        pass


class LooperReloader(threading.Thread):
    def __init__(self, looper, *args, **kwargs):
        super(LooperReloader, self).__init__(*args, **kwargs)
        self._n_reloads = 0
        self.daemon = True
        self.updated = False
        self.running = False
        self.looper = looper
        self._cached_reloader = CachedReloader()

    def update(self):
        self.updated = True

    def run(self):
        self.running = True
        self.looper.init_once()
        self.looper.init()
        self._loop()
        print "Thread exit"

    def _do_update(self):
        print "Updating"
        # Tell the reloader to flush its cache
        self._cached_reloader.new_generation()

        # Reload the looper class
        m = reload(inspect.getmodule(self.looper))
        lc = m.__getattribute__(self.looper.__class__.__name__)
        lc_instance = lc()
        old_lc_instance = self.looper.__class__()

        # Reload the main function
        self.looper.loop_body = lc.loop_body.__get__(self.looper, lc)

        # Reload the initialisation arguments
        lc_instance.init()
        old_lc_instance.init()
        for name, value in vars(lc_instance).items():
            if inspect.isroutine(value):
                try:
                    if inspect.getsource(value)\
                       != inspect.getsource(vars(old_lc_instance)[name]):
                        self.looper.__dict__[name] = value
                except KeyError:
                    # New function
                    print "KeyError"
                    self.looper.__dict__[name] = value
            elif isinstance(value, types.InstanceType):
                pass
            else:
                try:
                    if value != vars(old_lc_instance)[name]:
                        print name + " "\
                            + str(value) + ", "\
                            + str(vars(old_lc_instance)[name])
                        self.looper.__dict__[name] = value
                except KeyError:
                    # The property is a new one
                    print "property KeyError"
                    self.looper.__dict__[name] = value
        self.looper.__class__ = lc

        # Reload the rest
        for k, v in self.looper.loop_body.func_globals.items():
            if k != "__builtins__":
                if isinstance(v, types.ModuleType):
                    if not v.__name__.startswith("pygame"):
                        self.looper.loop_body.func_globals[k]\
                            = self._cached_reloader.get_module(v)
                elif isinstance(v, types.FunctionType):
                    m = self._cached_reloader.get_module(inspect.getmodule(v))
                    new_v = m.__getattribute__(v.__name__)
                    self.looper.loop_body.func_globals[k] = new_v
                elif isinstance(v, types.InstanceType):
                    print "object"

    def _loop(self):
        while self.running:
            try:
                if self.updated:
                    self.updated = False
                    self._do_update()
                if self.looper.loop_body():
                    self.running = False
            except Exception:
                traceback.print_exc()
                time.sleep(5)
~~~~

## watcher.py
This file provides a class that is used to notify the reloader of any changed
files.

~~~~{.python .numberLines}
from watchdog.events import FileSystemEventHandler


class Notifier(FileSystemEventHandler):
    def __init__(self, reloader):
        super(Notifier, self).__init__()
        self.reloader = reloader

    def on_modified(self, event):
        self.reloader.update()
~~~~

## main.py
This file is the script that is used to run the code provided in appendix A

~~~~{.python .numberLines}
import instapy
import watcher
import time

import demo


if __name__ == "__main__":
    r = instapy.LooperReloader(demo.Game())
    handler = watcher.Notifier(r)
    o = watcher.Observer()
    o.schedule(handler, path='.', recursive=True)
    o.start()
    r.start()
    try:
        while True:
            if not r.is_alive():
                break
            time.sleep(1)
    except KeyboardInterrupt:
        pass
    finally:
        r.running = False
        o.stop()
        o.join()
        r.join()
~~~~

\newpage

# Bibliography
