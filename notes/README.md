% Interactive Game Programming in Python
% Kit Barnes
% MInf Project

> This project is an endeavour to create a system for instant feedback while
programming, completely removing any semblance of a compile step. Following an
implementation of this a report will be produced comparing the implementation
to similar systems with respect to how they improve programmer productivity and
to what degree they do so.
> The project will focus on producing 2D games using a fixed set of
libraries and a newly created framework that will provide the desired
functionality; given time this framework will include an editor component that
enables more granular control of the mechanisms of the project.

# Interactive programming
The overall goal of this project is to produce a system by which a programmer
can write their code while that same code is running and any changes to the
source code will be reflected as soon as possible in the running code. Changes
will not be made whilst the source code is syntactically invalid and any errors
produced by the running code should cause the system to roll back to a
previously working version.

By its nature this will require the running system to be a long-running
process; almost certainly a system with a main loop of some description. For
this reason -- as well as to provide a focus -- I have selected game
programming as the application of this system, though hopefully much of the
code will be applicable to more general systems.

# Evaluation focus
I will dedicate my time to producing the software as effectively as possible.
Then to evaluate the software I will compare it to other projects with a
similar aim. That aim will be improving programmer productivity and creativity
by providing instant feedback on changes made.

# Requirements for a two-dimensional game
Each of the following things is a feature that the framework will need to
provide to allow programmers using it to produce as wide an array of games as
possible:

* A graphical rendering mechanism
    - A movable camera, such that the game can take place in an area larger than
      the users' screens.
* An audio output mechanism
* A mechanism for simulating physics.
* An easy way to provide collision detection
* A user interface module, providing simple controls and textual input
* A variety of input mechanisms -- both keyboard and mouse at the very least;
  the mechanism should be extensible so that programmers could add their own
  control mechanisms if they so desire (such as accelerometers or Kinects)

# Library selection

## Graphics/Game library
There are two widely used two-dimensional graphics libraries available for
Python. They are both geared towards game programming and also provide audio
output and generic input mechanisms:

### Pygame
I already have experience with Pygame and programming with it is relatively
simple. It provides a few prebuilt shape drawing primitives which are not
available with pyglet. There is a library for Pygame called pgu that provides a
GUI toolkit and some helper libraries for tile-based games (a common subset of
2d games).

Unfortunately Pygame doesn't work with PyPy so if I decide to use PyPy then I
will have to use pyglet. Pygame also has difficulty using the GPU for rendering
on many platforms and so can be slow, though if used effectively it can be
easily fast enough.

### Pyglet
Pyglet works in PyPy (useful if PyPy seems necessary for this project) and
provides hardware acceleration on all platforms (meaning a programmer creating
a particularly complex game won't have to worry so much about performance).
There are UI libraries for pyglet and it appears to have a built-in text
editing component which may be useful if I make a GUI using pyglet.

Pyglet programming is low-level, essentially requiring the user to use OpenGL
primitive operations or an API not far abstracted from them. Pyglet has few
shape drawing functions that are useful for basic graphics.

### Decision
For the time being I'll be working with Pygame, though I'll try to remain
flexible and prepare for a switch to pyglet if necessary.

## Physics and collision detection
As with graphics libraries, Python has two widely used physics libraries:
Chipmunk and box2d. Chipmunk is the more recent and actively developed library.
I have prior experience with Chipmunk and the two libraries seem to be fairly
similar so I'll be using Chipmunk.

# Example modifiables
Some of the things that the programmer will typically need to be able to modify
interactively will be:

* **Physical constants**: Such as gravity
* **Control mechanisms**: Such as how *exactly* a player character might move
  -- this could be by applying an impulse, accumulating a force, or even
  directly increasing velocity
* **AI**: Such as making computer controlled opponents go from standing still
  to moving towards the player.

