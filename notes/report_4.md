% Interactively Programming Games
% Kit Barnes
% 14 October

Last year a system was developed that automatically updates a running Python
program with new code as that code is written. This system was demonstrated by
iteratively developing a small 2D video game and, though the system could be
used in a variety of applications, 2D video game development was the focus of
the system.

Continuing development on this system will focus on two largely orthogonal goals:

* Allowing the system to perform a larger set of transformations than it
  currently can.
* Improving the interaction experience with the system, including more
  fine-grained control of when transformations should occur.

# Transforming more code
The system as it stands is capable of transforming only a subset of the Python
language. This subset will be expanded throughout the year:

## Planned patterns for transformation
### Changing values in objects in arbitrary structures
Though the system works on a particular `Looper` object at the moment, this
object is a special case and is only used to allow for easier control of
Python's scoping mechanisms. The system will fail to appropriately transform
arbitrary objects. The solution to this is to adapt the current "God object"
transformation code to apply to arbitrary objects and to recurse through an
object reference tree. This will involve graph traversal and loop detection.

What this will fail to do is apply changes to the structure of the objects. That
is to say if the object graph itself is modified; for example if we were to
change a lookup table from a list of pairs to a dictionary structure.

### Changing object structures
As described above a mechanism for updating the structure of objects will be
required. A potential mechanism for doing this would be to produce some
heuristic mechanism that would map names from the old version of the system
onto names in the new system (names are what we call references in Python). A
*very* basic understanding of machine translation suggests that that field might
be a fruitful place to search for such a mechanism as it works to extract
equivalences between entities.

### Threaded code transformations
In the current system a threaded program will likely encounter race conditions
and unexpected behaviours as a result of methods and objects being changed or
destroyed in the middle of execution. A simple fix for *somewhat* sensible
behaviour would be to synchronise and pause all threads before performing an
update but this may not update the code running in the threads. Further
investigation is necessary here.

### Smaller cases
There are a number of more specific types of change that will cause the system
to fail. For example any change in which an object is renamed. This would be
fixed with a more generic mechanism for detecting rough equivalences as
described above.

Other problems that may arise involve such things as modules written in C or
packages that are not stored in the local directory (e.g. packages installed
globally with `pip` or a package manager).

# Improving the interaction experience

# Timeline

# Overall aim for this year

- break down into objectives
- list potential challenges and proposed solutions
- timeline, weeks or months, from now until april
- list as many things to try as possible
- need to demonstrate why it will take a year
