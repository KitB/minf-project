% Project milestones
% Kit Barnes

> The tasks described are all to be done by the end of the given week

# Things to be done, need adding to milestones
* Summary (?)
* Decide Aim (?)

# Year 1
## Semester 1
### Week 7
* Flesh out the choice of libraries, perhaps decide between the pygame and
  pyglet
    - Careful to check that pyglet provides the necessary functionality other
      than just graphical rendering.
* Write the rest of the milestones

### Week 8
* Write a summary of every related project you find, do this on a day-by-day
  basis. Note anything that might be useful
* Decide between evaluation foci

### Week 9
* Research experimental methods
* Write evaluation criteria

### Week 10
* Prepare for the group meeting in week 11, practice

### Week 11
* Outline report

### Week 12
* 1st Draft

### Week 13

### Week 14
* 2nd Draft


## Semester 2
### Week 1
* Create a system by which a programmer could load and run the code in the ball
  drawing example and have the running code update when the programmer changes
  the code in the ways described in the corresponding explanation.md
* Final report draft

### Week 2
* Have all of the single file examples working as well as the ball drawing code

### Week 4
* Have the multiple file example updating after the programmer changes it in
  some way - not necessarily perfect
