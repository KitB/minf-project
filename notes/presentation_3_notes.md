# Slide 2
No compilation means you'll just save the file and the new code will be running
No-restart means that you don't have to run your game from the beginning

# Slide 3
I have a working system that can update running code when you save a file.
There are constraints: it doesn't work on objects. That is to say that you
can't update an object's methods and have the in-memory object update those
methods. Except in the very special case of the Looper object.

Demo goes here

Demo Notes
==========

Start at revision 444c.


Start by bouncing the ball around a bit.

Discuss: "This is my example program. It's a little toy game in which you can
bounce a little ball around."

Now change the radius, change the colour of the ball to white. Change the
position of the ball and see it reinitialise.

Discuss: "We can change the basic initial values such as the radius and colour
of the ball. Note that changing only one value will not reinitialise the other
values. To do this I create a fresh instance of the new class along with a
fresh instance of the old class during the update process and I call the init
function on each of these then I compare each instance's local variables."

Change the value of gravity to be negative; change it back, change the gravity
accumulator to negate gravity on line 67; change it back. Make "darken" even
darker.

Discuss: "We can also change the behaviour of arbitrary functions and methods
of this class."

Add the gravity inverting control on line 96

Discuss: "Now I'll demonstrate that it can produce this example incrementally
from almost nothing. First I'll switch to the minimal version of the file."

Git stash.
Checkout revision 2fcc.

Discuss: "And run the system here. I'll be hopping through the stages quickly
so you won't get much of a chance to see the code but it is important that you
know that at each stage the command I am running is only writing the file. This
has no direct interaction with the running system."

`python test.py`

Discuss: "As you can see we aren't drawing anything. Just busy-looping on a
black screen. This code is the minimal code that will allow us to produce the
example I was modifying before. These few lines are required because I cannot
re-run `pygame.init()` or create a new screen object more than once. At the
moment the only mechanism for ensuring this is to insert such statements into
the `init_once` function."

next checkout.

Discuss: "Now we have some code drawing the ball to the screen. But it looks a
bit boring"

next checkout.

Discuss: "Much better. We added a new function "darken" to do this. Now lets
get it moving."

next checkout.

Discuss: "We've added both motion and damping here, hence the ball slowing
down. The motion is so very slow simply because I don't want the ball to drift
off of the screen until we've implemented bouncing. Before that though, lets
add gravity"

next checkout.

Discuss: "Again, gravity is low because I want it to stay on the screen. Also
in aid of this goal I'll be moving on quickly"

next checkout.

Discuss: "Now we've increased gravity to a good-looking approximation of real
life and added bouncing as you can see."

change the initial position to demonstrate bouncing again

Discuss: "Finally lets add interactive control and we'll be at the point we
started at at the beginning of the demo."

next checkout. make the ball jump around a bit.

Discuss: "As you can see we're at the state we were in at the beginning. We can
do some cool things from here like making the ball change colour and having
Angry Birds style controls."

checkout `more_interaction`


End Demo
==========

# Slide 4
The next step in the implementation will be to ensure that the system works
properly for multiple source files that reference each other.

# Slide 5
I hope to add support for updating objects by the end of the semester *but* not
including state transformations (if you change your data structure then the
system won't be able to deal with it).

Next semester I'll be working on adding state transformation (so you can make
changes to your data structures).
