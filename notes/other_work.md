% Related Projects
% Kit Barnes - s0905642
% MInf Project

# E15:oGFx
[E15:oGFx](http://ogfx.mitplw.com/) is a tool that uses an embedded Python
interpreter to provide an interactive graphical programming experience. I shall
have to investigate this project to see if it is open source.

# Dynamic Software Updating
Hicks presents a mechanism for updating a running process with a new compiled
version of the software. His approach requires programmers to write in a new
language (albeit a language similar to C) but imposes very little overhead in
performance. As Python is already an interpreted language I will likely suffer
little overhead with relatively simple code replacement mechanisms. A great
advantage of this system is a mechanism for automatically generating "dynamic
patches", which include a state transformer function. I shall have to
investigate this mechanism further.

# Erlang
Erlang's hot code loading mechanism provides (as far as I can tell) a means for
providing new code to run and state transformers to allow that code to run. It
does not appear to provide any mechanism to ease the process at all and Erlang
code must be written in a particular style to be able to make use of this
functionality. (an approach I might end up using)

# Bret Victor
Bret Victor demonstrated a number of interactive coding environments in the
talk that inspired this project, all of them thoroughly tailored to a specific
task and with no code available to work from. Brett has released a library that
performs a similar task, though not the same by any stretch.

## Clones
### [Livecoding](http://livecoding.io)
An implementation of the graphical code update demo, includes the draggable
bar and colour editor.

### [CodeBook](http://danielchasehooper.github.com/CodeBook/)
Another implementation of the graphical code update demo, includes draggable
numbers and inspection (where hovering a pixel highlights the line that drew
it).

# Scratch
More of an educational tool -- No longer a focus.

# Alice
Also educational software.

# [Fluxus](http://www.pawfal.org/fluxus/)

# [Python Livecoding](http://pypi.python.org/pypi/livecoding/2.02)
A prebuilt environment that looks to be very similar to this project, I will
almost certainly be borrowing code and/or concepts from this project.
