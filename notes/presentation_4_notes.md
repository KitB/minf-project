I have no slides as my system lends itself more to being demonstrated than
screenshotted.

I'll start with a quick demo of the system for those of you who don't know what
I'm making and to jog the memory of those who have:

Demo Notes
==========

Start at revision ba2fe2e55.


Start by bouncing the ball around a bit.

Discuss: "This is the example program I used last time. It's a little toy game
in which you can bounce a little ball around. My system allows us to change the
code in real time and have the running example reflect these changes like so:"

Now change the radius, change the colour of the ball to white. Change the
position of the ball and see it reinitialise.

Discuss: "We can change the basic initial values such as the radius and colour
of the ball. Note that changing only one value will not reinitialise the other
values. To do this I create a fresh instance of the new class along with a
fresh instance of the old class during the update process and I call the init
function on each of these then I compare each instance's local variables."

Change the value of gravity to be negative; change it back, change the gravity
accumulator to negate gravity on line 67; change it back. Make "darken" even
darker.

Discuss: "We can also change the behaviour of arbitrary functions and methods
of this class."

Add the gravity inverting control on line 94

Discuss: Last time I demonstrated building this example up from scratch with it
all running.

Close it, switch to pokebrawl.

Run pokebrawl.

Discuss: This is a more advanced example of the kind of games I would like this
system to be able to perform transformations on. The difficulty arises because
this game makes extensive use of objects and includes a library written in C for
physics, as well as more complicated uses of pygame (for example loading the
background image). Further improvements to this game's code will include
splitting the physics, sound, and display code into threads; a change which will
cause further difficulties for the system.

# Back to talking
I have two primary aims for the system in the coming year:

The first and foremost is to increase the coverage of Python that can be
transformed. For example this will include making the system successfully
transform object-heavy code and code that uses threads. There also exist
annoying corner cases for the system, such as transformations in which a thing
is simply renamed. There are a lot of cases like these and the fixes for some of
them are large. For instance the fix for renaming objects is to define some
fuzzy equality metric that can detect an object that has both changed slightly
in value *and* been renamed. Many of the fixes will be heuristic and improving
the behaviour of the system when transforming these edge cases will take a fair
while.

> If asked for more examples of such edge cases:
> Over the weekend I found a bug that would occur if I used `math.pi` in the
> `init` function of a game and then ran the update mechanism on it twice. Even
> with no changes. It turned out that when I tried to initialise a fresh
> instance of the old class for comparison it had lost all imported modules
> because I had deleted them to prevent a memory leak. I now keep a limited
> history of imported modules to prevent this from happening. This took me 5
> hours of solid work to fix. I don't know about you but for me that's pretty
> much as many hours of focussed work as I can do in a single day.

The second aim is to improve the experience of interacting with the system; in
its current form the only interaction is performed by saving a file. In future I
would like to produce a simple GUI that will allow the user to, for example,
pause the updates so that they can save something incomplete and continue to
play their game. Or a state reset that would, in the example given earlier, move
the ball back to the starting position with the starting velocity etc.

I will continue on from there to produce a plugin for some text editor, for
example vim or sublime text or gedit. Maybe more than one. This plugin would
provide an interface to the same functions as the basic GUI but it would also
allow for the system to perform an update when the user stops typing, rather
than when the user saves.

On wednesday I will be giving a workshop on writing games in Python to a group
of under-19s. I hope to use my project in that workshop and, as a tertiary
objective for the project, will produce educational materials that would allow
someone else to run a similar workshop using the system.
