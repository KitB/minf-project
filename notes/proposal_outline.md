% MInf Proposal Outline
% Kit Barnes

# The proposal needs to include:
## Sections from the MPP page
* **Purpose**: Statement of the problem and justification of why it should be
  solved
    - Problem: "There is a compile step when programmers are writing games"
    - Justification: "There is a furvor on the internet at the moment about
      this as a result of Brett Victor's talk" -- that's a joke; I have no idea
      how to justify this.
* **Background**: Literature survey and evaluation of applicability to the
  exact problem stated
    - Review literature
* **Methods**: A description of how to test your "hypothesis" -- my hypothesis
  is "I can produce a system that allows a programmer to have their code
  running whilst they write it and have that running code update in a
  meaningful way as they write it"; I can only think of one way to test that
  hypothesis.
* **Evaluation**: How will you know how good your output will be?
* **Outputs**: What will you be producing?
* **Workplan**: A timetable
    - Risk analysis and contingency planning
    - Detailed implementation time plan


## Things I'd like to put in
* Justify anticipated implementation methods and technologies


## Sections I think I'll have
* **The problem**: Description of the problem and why it needs solving
* **Literature survey**: Including a short section summarising and evaluating
  the sum of previous work in this direction.
* **My solution**: A description of the system I intend to produce.
    - Justifying anticipated implementation methods and technologies
* **Identifying the project** -- Could conclude with the aims and objectives
* **Specification and Evaluation**: I want to be able to do these things in
  this way.
* **Planning**: A detailed time plan and some risk analysis and contingency planning
