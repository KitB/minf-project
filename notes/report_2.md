% Interactively Programming Games
% Kit Barnes
% 26th November

# Introduction
> This project is an endeavour to create a system for instant feedback while
programming, completely removing any semblance of a compile step.
> The project will focus on producing 2D games using a fixed set of
libraries and a newly created framework that will provide the desired
functionality.

# Project aim and objectives
The aim of this project is to produce a system by which a programmer
can write their code while that same code is running and any changes to the
source code will be reflected as soon as possible in the running code.
The objectives are:

* Produce a library that allows replacement of code.
* An example of an interface that uses this library
* Refine the library to allow state transformation
* Add automatic inference of state transformations

# Progress
In the last meeting my options for the project were very broad and I had some
decisions to make to narrow the scope of the project. I have made these
decisions; such as focussing on game programming and producing a library that
will allow me to shape the kinds of interactions a programmer can have with the
system, rather than allowing arbitrary interactions.

I have produced a small suite of example programs including descriptions of the
interactions a programmer can expect to be allowed to do with the final system.

I have defined the API I expect to be implementing, though I am aware there is
a good chance this will change as I write it.

I have researched projects related to code updating, be it live or otherwise.
For each of these projects that I have spent enough time with to be able to
understand it I have written a short description of what it does and which
parts of it may be relevant to my own project.

# Current plan
The final system will include an API that will provide the following methods:

* `watch`: Begins bookkeeping on a module to ensure that state changes are
  performed sensibly
* `start`: Begins execution, this will probably accept a function as input
* `pause`: Halts execution in such a way that it can recommence, I have no idea
  how to implement this right now so I might not include it
* `stop`: Halts execution entirely
* `update`: Taking in a new source code for a module, this will cause that
  module to be updated

I will also provide an example interface that uses the system consisting of a
file watching script that will call the update function when a file is saved,
allowing the programmer to use their own text editor.

Challenges in implementing this will be:
a. Providing inference of state transformation
b. Ensuring that the system performs sensible actions in cases where no obvious
   solution exists -- for instance what the system should do when the running
   code raises an exception.

\newpage

# Time plan
## Week 11
* An outline of the proposal
    * An ordered list of sections and a description of what each description will contain
* Continued research into similar projects
    * Scratch, Alice, Fluxus

## Week 12
* Parts of the first draft of the proposal
    * Finalise proposal outline
    * Risk analysis and contingency planning
    * Review literature

## Week 13
* Fully completed first draft
    * Detailed implementation time plan
    * Justify anticipated implementation methods and technologies
    * Completed research on evaluation methods

## Week 14
* A refined second draft
    * Taking suggestions from my supervisor
    * Continued review of relevant literature

## Semester Two -- Week 1
* A final draft
* The most rudimentary interactive programming system I can produce, allowing
  the first ball example to be modified in the specified ways

## Week 2
* The final report -- due on the 25th January

## Week 3
* A system (of any description, not necessarily with an API - perhaps buggy)
  that will allow the proposed transformations for all of the single-file
  examples

## Week 4
* A system that allows the multiple-file example to be modified as proposed.

## Week 7
* An outline of the phase one report

## Week 9
* A first draft of the phase one report

## Week 12
* Phase one report hand in
