% Log of interesting things during implementation

# Module in `init`
I found a bug that occured if I used a value from a module in a Looper's `init`
method then updated it twice. The issue would occur because I was deleting my
internal module list when I started a new generation in my CachedReloader.
Apparently the reference in the fake module was not counted and the module was
garbage collected?

Fixed by keeping a history of generations. Currently have full history but will
switch to a deque to avoid the inevitable memory leak. This will lead into
historical checkpointing.

~5hrs work to fix

# Importing modules from packages
Any import of the form `import package.subpackage.module` or
`from package.subpackage import module` would fail to be updated. This was as
the `find_module` function from the `imp` module of Python's standard library
does not handle dotted module names. The `find_module` function can take an
extra `path` argument which contains a list of directories in which to search
for the given module. This means it was easy enough to extract the directory
path from the package name by replacing dots with slashes. The final call for
the above import string would look something like:

`imp.find_module('module', ['package/subpackage'])`

Additionally, packages can contain a `__init__.py` module that is actually
imported if you run `import package`. The `find_module` function does not
account for this, so an additional code path that recognises a package import
was added to such that for the code `import package` it would run
`imp.find_module('__init__', ['package'])`

~7hrs work to fix
