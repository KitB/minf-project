# File gravity.py
# Adds a constant downward acceleration to the ball
import pygame


# Some colours
black = (0, 0, 0)
purple = (116, 4, 181)

pygame.init()
screen = pygame.display.set_mode((640, 480))
clock = pygame.time.Clock()

ball_pos = (120, 120)
ball_velocity = (0, 0)
gravity = 150
friction = 0.5
radius = 40


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)


def draw():
    pos = [int(round(n)) for n in ball_pos]
    pygame.draw.circle(screen, darken(purple), pos, radius)
    pygame.draw.circle(screen, purple, pos, radius, 1)


def update_ball():
    global ball_pos, ball_velocity
    dt = clock.tick(60)
    dt_seconds = dt / 1000.0
    ball_pos = [a + (b * dt_seconds) for (a, b) in zip(ball_pos, ball_velocity)]
    ball_velocity = [n * (1 - (friction * dt_seconds)) for n in ball_velocity]
    ball_velocity[1] += gravity * dt_seconds


done = False
while not done:
    update_ball()
    screen.fill(black)
    draw()
    pygame.display.flip()
