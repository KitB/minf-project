% Single File, functions only
% Kit Barnes - s0905642

> These files are each self-contained and have no objects. They step through
the process of creating an interactive ball-bouncing demo using pygame for
rendering.

> A large disadvantage of pygame is that it doesn't work in PyPy at the moment.
I may consider switching to pyglet as the focus for graphical output in the
future of this project.

> These files are simple because they do not require me to also track changes
in imported modules. I foresee some difficulty in determining which modules a
user wishes to have automatically change as some imports may be system modules
whereas others may be part of the user project.

# Expected interaction
In the earliest form of the project the programmer will be able to modify the
constants set out at the top of the files. This will allow them to modify the
radius of the ball, gravity's pull, the elasticity of the ball, etc.

The next thing a programmer will be able to modify will be the functions in the
files, allowing them to change the way a user interacts with the ball or maybe
make the ball render as a square.

Something I'm not sure I will be able to make modifiable without getting into
the interpreter internals will be unnamed lines outside of functions or
classes. For example the contents of the `while` loop in all of these files.
This is because these lines are not accessible to python code for replacement
as far as I am aware.
