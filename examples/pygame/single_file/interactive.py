# File: interactive.py
# Allows the user to control the ball
import pygame
import pygame.gfxdraw
from pygame.locals import *


# Some colours
black = (0, 0, 0)
purple = (116, 4, 181)

pygame.init()
dims = (640, 400)
screen = pygame.display.set_mode(dims, pygame.HWSURFACE & pygame.DOUBLEBUF & pygame.FULLSCREEN)
clock = pygame.time.Clock()

ball_pos = (120, 120)
ball_velocity = (800, -400)
gravity = 900
friction = 0.8
elasticity = 0.7
radius = 40
stick = False


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)


def draw():
    pos = [int(round(n)) for n in ball_pos]
    pygame.gfxdraw.filled_circle(screen, pos[0], pos[1], radius, purple)
    pygame.gfxdraw.filled_circle(screen, pos[0], pos[1], radius - 5, darken(purple))


def update_ball():
    global ball_pos, ball_velocity
    dt = clock.tick(60)
    dt_seconds = dt / 1000.0
    ball_pos = [a + (b * dt_seconds) for (a, b) in zip(ball_pos, ball_velocity)]
    ball_velocity = [n * (1 - (friction * dt_seconds)) for n in ball_velocity]
    ball_velocity[1] += gravity * dt_seconds
    left = top = radius
    right = dims[0] - radius
    bottom = dims[1] - radius
    if ball_pos[1] <= top:
        ball_pos[1] = top
        if not stick and ball_velocity[1] < 0:
            ball_velocity[1] *= -elasticity
        elif stick:
            ball_velocity[1] = 0
    elif ball_pos[1] >= bottom:
        ball_pos[1] = bottom
        if not stick and ball_velocity[1] > 0:
            ball_velocity[1] *= -elasticity
        elif stick:
            ball_velocity[1] = 0

    if ball_pos[0] <= left:
        ball_pos[0] = left
        if not stick and ball_velocity[0] < 0:
            ball_velocity[0] *= -elasticity
        elif stick:
            ball_velocity[0] = 0
    elif ball_pos[0] >= right:
        ball_pos[0] = right
        if not stick and ball_velocity[0] > 0:
            ball_velocity[0] *= -elasticity
        elif stick:
            ball_velocity[0] = 0


def handle_events():
    global stick, done
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            keys = pygame.key.get_pressed()
            if keys[K_w]:
                ball_velocity[1] -= gravity
            if keys[K_s]:
                ball_velocity[1] += gravity
            if keys[K_a]:
                ball_velocity[0] -= gravity
            if keys[K_d]:
                ball_velocity[0] += gravity
            if keys[K_SPACE]:
                stick = True
            if keys[K_q]:
                done = True
        elif event.type == KEYUP:
            keys = pygame.key.get_pressed()
            if not keys[K_SPACE]:
                stick = False


done = False
while not done:
    handle_events()
    update_ball()
    screen.fill(black)
    draw()
    pygame.display.flip()
