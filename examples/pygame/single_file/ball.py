import pygame


# Some colours
black = (0, 0, 0)
purple = (116, 4, 181)

pygame.init()
screen = pygame.display.set_mode((640, 480))

ball_pos = (120, 120)
radius = 40


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)


def draw():
    pygame.draw.circle(screen, darken(purple), ball_pos, radius)
    pygame.draw.circle(screen, purple, ball_pos, radius, 1)

done = False
while not done:
    screen.fill(black)
    draw()
    pygame.display.flip()
