#!/usr/bin/env python2
import pygame
import pygame.gfxdraw
import pygame.font
from pygame.locals import *

from pokebrawl import control, frame


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)

# Some colours
white = (255, 255, 255)
black = (0, 0, 0)
purple = (116, 4, 181)

pygame.init()
dims = (1280, 800)
screen = pygame.display.set_mode(dims)
clock = pygame.time.Clock()
frame = frame.Frame(screen)

ubuntu_font_file = pygame.font.match_font('ubuntumono')
ubuntu_font = pygame.font.Font(ubuntu_font_file, 30)

background = pygame.image.load('bg.jpg').convert()
frame.set_background(background)

gravity = 1000
friction = 0.8
elasticity = 0.7
actual_fps = 60.0
stick = False


class Ball(object):
    def __init__(self, (x, y), (vx, vy), radius):
        self.x, self.y = (x, y)
        self.vx, self.vy = (vx, vy)
        self.radius = radius

        r = self.radius
        self._surface = pygame.Surface((r * 2, r * 2))
        self._surface.set_colorkey((0, 0, 0))
        pygame.gfxdraw.filled_circle(self._surface, r, r, self.radius, purple)
        pygame.gfxdraw.filled_circle(self._surface, r, r, self.radius - 5, darken(purple))

    def set_position(self, pos):
        self.x, self.y = pos

    def get_position(self):
        return (self.x, self.y)

    def set_velocity(self, v):
        self.vx, self.vy = v

    def get_velocity(self):
        return (self.vx, self.vy)

    def draw(self):
        return (self._surface, self.x, self.y, 1)


class Enclosure(object):
    def __init__(self, dims):
        self.dims = self.w, self.h = dims
        self._surface = pygame.Surface(self.dims)
        self._surface.set_colorkey((0, 0, 0))
        pygame.draw.rect(self._surface, purple, (0, 0, self.w, self.h), 10)

    def draw(self):
        return (self._surface, 0, 0, 0)


class TextVar(object):
    def __init__(self, initial_text, font, colour, position):
        self.text = initial_text
        font_file = pygame.font.match_font(font)
        self.font = pygame.font.Font(font_file, 30)
        self.colour = colour
        self.x, self.y = position

    def set_text(self, text):
        self.text = text

    def draw(self):
        t = self.font.render(self.text, True, self.colour)
        return (t, self.x + frame.x, self.y + frame.y, 10)


ball = Ball((120, 120), (800, -400), 40)
enclosure = Enclosure((14000, 300))
fps_text = TextVar("60.0 fps", 'ubuntumono', white, (0, 0))
frame.add_drawable(ball)
frame.add_drawable(enclosure)
frame.add_drawable(fps_text)


def update_ball():
    global ball, frame, fps_text
    if abs(ball.vx) < 1:
        ball.vx = 0
    if abs(ball.vy) < 1:
        ball.vy = 0
    dt = clock.tick(60)
    fps_text.set_text("%3.1f fps" % (1000 / float(dt)))
    dt_seconds = dt / 1000.0
    ball.set_position([a + (b * dt_seconds) for (a, b) in zip(ball.get_position(), ball.get_velocity())])
    ball.set_velocity([n * (1 - (friction * dt_seconds)) for n in ball.get_velocity()])
    ball.vy += gravity * dt_seconds
    left = top = 0
    right = enclosure.w - (ball.radius * 2)
    bottom = enclosure.h - (ball.radius * 2)
    if ball.y <= top:
        ball.y = top
        if not stick and ball.vy < 0:
            ball.vy *= -elasticity
        elif stick:
            ball.vy = 0
    elif ball.y >= bottom:
        ball.y = bottom
        if not stick and ball.vy > 0:
            ball.vy *= -elasticity
        elif stick:
            ball.vy = 0

    if ball.x <= left:
        ball.x = left
        if not stick and ball.vx < 0:
            ball.vx *= -elasticity
        elif stick:
            ball.vx = 0
    elif ball.x >= right:
        ball.x = right
        if not stick and ball.vx > 0:
            ball.vx *= -elasticity
        elif stick:
            ball.vx = 0

    frame.x = ball.x - (dims[0] / 2)
    frame.y = ball.y - (dims[1] / 2)


def press(direction):
    power = 1800

    def inner_func():
        if direction == "up":
            ball.vy -= power
        elif direction == "down":
            ball.vy += power
        elif direction == "left":
            ball.vx -= power
        elif direction == "right":
            ball.vx += power
    return inner_func


def stop_ball():
    global ball
    ball.set_velocity([0, 0])


def enable_stick():
    global stick
    stick = True


def disable_stick():
    global stick
    stick = False


def quit():
    global done
    done = True

c = control.Control()
c.register(K_w, press("up"), control.ON_PRESS)
c.register(K_a, press("left"), control.ON_PRESS)
c.register(K_s, press("down"), control.ON_PRESS)
c.register(K_d, press("right"), control.ON_PRESS)
c.register(K_j, stop_ball, control.WHILE_PRESSED)
c.register(K_SPACE, enable_stick, control.ON_PRESS)
c.register(K_SPACE, disable_stick, control.ON_RELEASE)
c.register(K_q, quit, control.ON_PRESS)
done = False
while not done:
    c.handle_events()
    update_ball()

    frame.render()
