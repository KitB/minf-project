import pygame

bg_scroll = 0.2


class Frame(object):
    def __init__(self, screen):
        self.objects = []
        self.screen = screen
        self.following = None
        self.x, self.y = 0, 0
        self.w, self.h = screen.get_size()
        self.background = None

    def add_drawable(self, drawable):
        self.objects.append(drawable)

    def get_position(self):
        return (self.x, self.y)

    def set_background(self, background):
        self.background = background

    def render(self):
        if self.background is not None:
            self.screen.blit(self.background, ((bg_scroll * -self.x) - 150, (bg_scroll * -self.y) - 100))
        layers = {}
        for obj in self.objects:
            (surface, x, y, z) = obj.draw()
            try:
                layers[z].append((surface, x, y))
            except KeyError:
                layers[z] = [(surface, x, y)]

        for key in sorted(layers.keys()):
            for (surface, x, y) in layers[key]:
                self.screen.blit(surface, (x - self.x, y - self.y))

        pygame.display.flip()
