% Multiple file, some classes
% Kit Barnes - s0905642

> This folder presents a more realistic Python program with some submodules
which may themselves be updated.

> The challenge here is in deciding which imported modules to keep track of.
`main.py` also imports a number of pygame modules which we don't want to track
for changes.

# Expected interaction
The programmer will be able to modify all of the functions and values in all
three modules in this folder. This includes methods and class members.
