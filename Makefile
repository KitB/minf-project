all: mpp1.md proposal.md

proposal.md:
	./make_md.sh proposal/proposal.md

mpp1.md:
	./make_md.sh mpp1/mpp1.md

clean:
	for f in `find . -name '*.pdf'`; do rm $$f; done

reports:
	for f in `find . -name '*.md'`; do ./make_md.sh $$f; done
