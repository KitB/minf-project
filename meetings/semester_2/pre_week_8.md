I have prepared some notes on what I shall be writing in my mpp1 report, though
I have not given them sections just yet:

# Project and Goals

## The problem
There are three problems that feed into this project, though it focusses on
just one of these problems:

* Compilation and running is an interruption that removes the programmer from
  the code briefly. I'll cite research on how the smallest interruptions kill
  productivity in any knowledge task, particularly programming.
* Reproducing the state of a long running process manually requires time.
  Automating it is harder than just serializing and deserializing because
  things will have changed in the meantime.
* Exploratory coding in creative works and processes is stinted by the
  compile/run cycle. Mention how programming is often considered a creative
  practice in general. Compare with image editing: ImageMagick vs Photoshop
  etc. Keep grounded with the game example.

## The solution
For each of these problems, here is a potential solution:

1. Automatically run code as seamlessly as possible
    - For example an eclipse run configuration that is run whenever the
      programmer saves.
    - In functional programming this could run just the current function
2. Dynamic Software Updating in the style of Hicks et al
    - For example if you have a game server (long-running, thoroughly stateful)
      that you wish to update to a new production version without restarting
      it.
    - Or the updating the kernel of your operating system without restarting!
3. A combination of the above two
    - For example a game that changes as you write the code.

Solution one should be relatively simple, perhaps as an eclipse plugin.
Solution two has been done by Hicks et al. Solution three is the focus of the
project.

# Your approach
This section mostly empty for now.

1. Provide automatic code reloading
2. Add state transformation
3. Add state transformation inference

# Work Plan
## So far

* Reload on save
* Performs simple state transformation
    - Replaces functions with new ones
    - Modifies initialised values

By the time I hand this in:

    - Updates objects with new classes etc.
    - Possibly allows for user defined state transformations

## Next steps
* State transformation and inference
* A keystroke granularity interface
    - e.g. a plugin for sublime text, gedit, or vim


# Marking criteria
## Critical evaluation
- Talk about how I'm imposing restrictions upon the programmer and how I might
  be able to fix this.
- Talk about how the transformation inference library might be a bad idea?
    * as opposed to ...?
