Opportunity to go into detail about what I've done.

Critical evaluation is just a roughly objective evaluation. Say how well what
you've done fits into what you said in the first report.

e.g. At the beginning you were planning to do it for general code, now you have
narrowed it down to games.

In your first proposal you had suggestions of how you may do it, you may not
have used those ideas.

Mention how the API has changed. Describe this in the "your approach" section.

Your approach is basically a description of the system I have.

Note difficulties in implementation, say if a bug takes more than an hour or so
to fix.

Be careful that when you show it off people don't think it takes no effort.
Make sure you explain what's going on under the hood.

Write it out and look at it (with regards to the question of "should I write
pseudocode to explain how it works")

10-12 pages (except those are 12pt, 2cm margin pages) but don't write for a
page count at first. Write what you think then adapt if necessary.
