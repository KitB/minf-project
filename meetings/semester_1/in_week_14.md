# The Problem
Don't call it "the problem"

Call it ["Project Purpose", "Motivation", "Aim", "Introduction"]

# Background
Classify them, say what they are like compare Victor with Hicks -- theoretical
vs practical, livecoding vs dsu.

Describe the things purely for what they do, don't put them in the context of
what I'm doing.

I need to inform the reader that I know lots about what's out there.

Example application domains for each.

# Identifying the project
Fine, says the right things, needs writing into text of course.

# Specification and evaluation criteria
Example code and transitions in that.

# Planning
Split into work plan and time plan, API details go into work plan.

For each month say what you're going to be doing on the implementation and
write up -- separate headers perhaps


# General
Write something every day, get words written. Set some time each day in which
you will not stop until you have, say, 300 words written. Could use 750words
for this.

Need to get SLIP done.


Don't forget to email Ian.

Refining examples -- good idea.

Flesh out later bullet points a bit more before you start writing full prose.
