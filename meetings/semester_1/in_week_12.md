% In meeting notes
% Kit Barnes
% Week 12

Need to revise slides and report.

Outline from guidelines on MPP web page, can be adapted.

Would be very good to present an outline of the MPP report.

Feel free to cut down what you think you might be able to do.

State a minimal usable API then improve in implementation.

Disjunction between the API that does live code updating and whatever gives
this API new code to update with -- so we can have a file watching example or a
text editor that auto-updates.


**Minimal Usable Interface**


# API
* Start
* Stop
* Pause?
* Update
* Watch module

*Don't worry about getting it wrong in the plan*


Set a deadline for the SLIP report. Maybe a couple of weeks.

By tomorrow: latest version of slides, progress report, draft outline.

Updates for the report:
Current situation should state "It will have this, it will do this"
Remove the bits in the "abstract" that shouldn't be there
List challenges -- "It's not just the code it's dynamic state"


More pictures in the report, perhaps diagrams of communicating components.


Proposal says: "Here is what I am going to do"
This will be things like:
"A minimal usable harness would be" -- Perhaps stories
Say very little about the implementation.

**Write the description so that somebody else could take it and implement it**
    * Be language agnostic
    * talk about using something like exceptions to roll back code to a
      previous state


Ask James and Craig for their slides and reports.
Drop the "research experimental method" todo for now, perhaps pick it up again
later on.

Could perhaps take the exception based roll back and make it a distinct
objective.

