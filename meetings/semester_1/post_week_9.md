% Report after meeting
% Kit Barnes
% Week Nine

# Time considerations
As the MInf project is worth one third of my marks for this year it should take
one third of my time, as such I will tentatively add Monday to my days to work
on the project solely.

I've also noted that in project work I should attempt tasks in decreasing order
of preference (eating the biggest frog first, so to speak).

# Concrete aims
Instead of saying things like "I will improve programmer productivity and
creativity" -- the potential results of producing my system -- I shall set my
bar more achievably at "I will produce a system for game programming that
provides instant feedback on changes made" (though perhaps with a little
elaboration)

# Other work/literature survey
Regarding livecoding I need to look into what mechanisms they provide for state
transformation as well as acquiring a general understanding of their
mechanisms. This should provide me with some insight for the planning process.

For the sake of this proposal, in particular the literature review I should
include projects that seem far removed from what I am attempting. This is as
they may also provide useful insights in parts of them that I might take on
board for my project.

Once I have completed the implementation I will be more selective about what I
compare my system to; this is as only some projects will be relevant enough to
be useful in a comparison.

# Examples
I need to include as many *types* of example as I can think of. These would be
things like:
* Going from having no code to having a small game
* Going from having a game to having a more complex game -- What I already have
* Going from having a game to having a rather different game (Space Invaders to
  Asteroids, for example)

# Meeting preparation
In my presentation for the next meeting I should include:

* A demonstration of my example code, emphasizing that these are simply
  examples of what a programmer might be able to change with instant feedback
  in the final system.
* Further examples of what a programmer might be able to change (particularly
  with regard to state transformation and introduction perhaps)
* A demonstration that I have made the necessary decisions and focussed my
  work.
* A fine grained plan with weekly, detailed steps to take until the proposal
  hand in. Followed by at least monthly plans until the end of the academic
  year.
* A description of the current literature

# State transformation tests
Ian suggested adding a colour state to a bouncing ball and having it change
colour whenever it bounces.

# Evaluation criteria
I am going to investigate similar work done by previous students to find good
criteria for evaluating a system for producing code.

I could also split the aim of the project into smaller objectives and list
completion of these as evaluation criteria. With the current aim of producing a
system that allows live coding of python games using pygame or pyglet example
objectives would be:


* Production of a library that allows replacement of code.
* An example of an interface that uses this library
* Refining the library to allow state transformation
* Add automatic inference of state transformation
