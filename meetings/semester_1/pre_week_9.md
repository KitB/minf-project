% Report before meeting
% Kit Barnes
% Week Nine

# Deciding between evaluation foci
I have decided to focus on writing the software, giving more time in my plan to
the implementation phase. In case I should complete the project in less time
than I give myself I can then go on to perform a more robust analysis of the
success of the software.

I've written more in-depth on this decision in README.md; also adding a more
detailed explanation of what the project is.

# Related work summary
Contained in `other_work.md`, I have outlined the relevance of a few related
projects. I fear this may be an ongoing task as it takes a fair while to
understand each project enough to know what it may provide me and how it is
different to what I am doing.

# Concerns about the python livecoding module
There exists a module for automatic reloading of code in Python, it appears to
support much of what I intend to do, though not necessarily in real time
(though I suspect it could be made real-time with only slight modification if
it was not). Is my project worthwhile with the existence of this module? What
am I doing that it doesn't do?

# Concerns about doing enough work
I have managed to find one or two hours prior to sunday this week to work on
the summaries but that wasn't nearly enough and I can only forsee less free
time in the coming two weeks (with a number of coursework deadlines and a SLIP
crunch period): am I going to be able to do enough work on my project? Perhaps
I need to dedicate more than just Sundays and whatever other time I can find to
the project. Mondays too?
