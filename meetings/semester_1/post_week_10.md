% Report after meeting
% Kit Barnes
% Week Ten

# Report and presentation
I will need to include in the report/presentation:

* A detailed timetable, including weekly deadlines up to the first handin
  and at least monthly up until the end of semester two. This timetable
  should include the deadline for the handin.
* A description of related work.
* Some detailed mockups of interactions with the system, perhaps user stories.
