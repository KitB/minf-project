% Report after meeting
% Kit Barnes
% Week Thirteen

# Proposal sections
I am to have written an outline such that I could reach from that outline the document to
be submitted by Friday.

Instead of a literature survey I'll have a section on the background or state
of the art. It will be named as such because many of the surveyed projects are
not literature and it will contain a description of only those projects I have
looked at so far. A true literature survey would presumably have a wider
variety of projects.

The evaluation section will also be the specification section as my evaluation
will simply be "to what degree does it meet the specification"

I will include a section "Identifying the project" that will describe the ways
in which I have focussed the project to prevent it from being too general. This
section may conclude with the aims and objectives I have specified. It will
also include such things as how I have decided upon an API and perhaps a
description of (or just suggestion that there need be?) a minimal usable
interface.

Regarding the suggested sections from the MPP website, I will replace the
hypothesis section with my specification (and evaluation) section. I will also
replace the method and experimental details section with my implementation
plan.

# Section details
My justification for some decisions will simply be "a decision had to be made
and I settled on this one".

The time plan should include some details of what I will be doing in the second
year of the project.

In my definition of the project I can motivate it by describing why other
people (i.e. the internet and related projects) think it is important --
including Bret Victor's talk.

# Other notes

* The proposal should be a document that:
    - Describes and motivates the project
    - Describes what I have done this semester
    - Demonstrates a plan for the project that is likely to lead to its success
* I need to begin writing early and repeatedly revise, rather than trying to
  get the essay correct on the first attempt
* Code listings and screenshots should be included in an appendix
