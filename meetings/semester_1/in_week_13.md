Why people think live coding is a useful technique

A more specific enumeration of systems that I have looked at -- this includes
things that turned out not to be relevant. Could include Bret Victor's work in
this section.

Sometimes justification will be "Because I needed to make a choice".

Include code listings as an appendix.

Perhaps a section on things like "setting the api", "deciding you need a minimal
demonstrator".

Distinction between setting the evalutation criteria and planning what you are
going to do.

Specification will be language agnostic -- except that it refers to the example
programs. Put the language specific details as far down as possible.

Implementation plan will say something about what I'm going to do in second
year.


Hypothesis -> Specification (contains evaluation)
    Includes the code samples and things like "we can change these parts", "we
    can get from this example to this example"
Method (experiment) -> Implementation plan


Write early, write often.

This write up is about telling them what I have done.


By Friday: an outline such that I could reach from that outline the document to
be submitted.
