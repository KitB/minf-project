% Report after meeting
% Kit Barnes
% Week Eight

# Deciding an aim for the project
I need to decide whether I want to focus the project on improving the
experience of programming or easing the learning of programming. I'm currently
leaning towards improving the experience of programming because it will allow
me to make more of my decisions based upon what I would like to use (as a user
of the system) rather than what I think is easiest for a new programmer to use.

Things I need to consider in making this decision:

* How am I going to evaluate the system when it is done, what will be easier to
  evaluate?
* What would I rather be working on for the next year and a half?

# Milestones to add
Some things I need to add to my milestones:

* Outline the project proposal, due for 25th January (week 2 of semester 2 I
  believe)
* Write a first draft of the proposal
* Write a second draft
* Write a final draft
* Complete the report
* Create mockups for the final UI
    - With user stories
    - Using example code and giving examples of what the user might change
* Write the ball example in other live coding environments

Ian informed me that there were actually 14 weeks available to me in semester one,
with the final three being somewhat dedicated to project work.

# API Details
I need to decide upon a sensible API for the system; even if I expect to change
it later it will likely be better if I have a solid starting position.

As it is my API consists of:

* A `watch` function that will take a python module (or potentially source file
  path) and cause the system to begin tracking that module for changes.
* An `update` function that will take a module and new source code (or
  potentially a diff) for the module and perform the live update, perhaps
  raising some sort of exception if the code is malformed.

I've also noted that it may be possible to wrap each module in a namespace such
that it can be swapped out with python's name assignment mechanisms (literally
the `=` operator).

# Reports
Ian has recommended that I write a report detailing each meeting we have after
the meeting to ease recollection of what we have discussed. I should also keep
Ian up to date more regularly than once a week with my findings and progress.

I've decided that I should begin attaching priorities to tasks and explicitly
ordering them and deciding upon a time in which I need to do them. This will
ease the process of getting to work, particularly in the three weeks dedicated
to this project in which I will need to enforce a structure on my work if I
hope to get as much work done as I would like to.

I will also consider writing each thing multiple times, refining it each time.
I fear there may have been something more attached to this "do each thing three
times" that I have forgotten.
